﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Text = UnityEngine.UI.Text;


public class StatisticsController : MonoBehaviour
{
    public static StatisticsController Instance { get; private set; }

    public Interface InterfaceComponent;

    private long _gameTimeAmount;
    public Text GameTimeAmountText;
    public long GameTimeAmount
    {
        get { return _gameTimeAmount; }
        set { _gameTimeAmount = value;
            GameTimeAmountText.text = ConvertLongToTimeFormat(_gameTimeAmount); SaveStaticticInPrefs(); }
    }

    private long _tapsAmount;
    public Text TapsAmountText;
    public long TapsAmount
    {
        get { return _tapsAmount; }
        set { _tapsAmount = value; TapsAmountText.text = _tapsAmount.ToString(); SaveStaticticInPrefs(); }
    }

    private long _mailsOpenedAmount;
    public Text MailsOpenedAmountText;
    public long MailsOpenedAmount
    {
        get { return _mailsOpenedAmount; }
        set { _mailsOpenedAmount = value; MailsOpenedAmountText.text = _mailsOpenedAmount.ToString(); SaveStaticticInPrefs(); }
    }

    private long _earnedMoney;
    public Text EarnedMoneyText;
    public long EarnedMoney
    {
        get { return _earnedMoney; }
        set { _earnedMoney = value; EarnedMoneyText.text = InterfaceComponent.FormattingValue(_earnedMoney); SaveStaticticInPrefs(); }
    }

    private long _spendMoney;
    public Text SpendMoneyText;
    public long SpendMoney
    {
        get { return _spendMoney; }
        set { _spendMoney = value; SpendMoneyText.text = InterfaceComponent.FormattingValue(_spendMoney); SaveStaticticInPrefs(); }
    }

    private string _gameVersion;
    public Text GameVersionText;
    public string GameVersion
    {
        get { return _gameVersion; }
        set { _gameVersion = value; GameVersionText.text = _gameVersion; SaveStaticticInPrefs(); }
    }

    private long _investmentsInSec;
    public Text InvestmentsInSecText;
    public long InvestmentsInSec
    {
        get { return _investmentsInSec; }
        set{ _investmentsInSec = value; InvestmentsInSecText.text = InterfaceComponent.FormattingValue(InvestmentsInSec); SaveStaticticInPrefs(); }
    }


    private DateTime lastTime;
    private int time = 1;

    private void Awake()
    {
        Instance = this;
        GetStaticticFromPrefs();
        GetGameVersion();
    }

    private void FixedUpdate()
    {
        GameTimer();
    }

    public void SaveStaticticInPrefs()
    {
        string statistic = string.Format("{0}/{1}/{2}/{3}/{4}/{5}/{6}", _gameTimeAmount, _tapsAmount, _mailsOpenedAmount, _earnedMoney,
            _spendMoney, _gameVersion, _investmentsInSec);
        PlayerPrefs.SetString("Statistic", statistic);
    }

    public void GetStaticticFromPrefs()
    {
        if (PlayerPrefs.HasKey("Statistic"))
        {
            string statistic = PlayerPrefs.GetString("Statistic");
            string[] stringSeparators = new string[] {"/"};
            string[] textArray = statistic.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

            GameTimeAmount = Convert.ToInt64(textArray[0]);
            TapsAmount = Convert.ToInt64(textArray[1]);
            MailsOpenedAmount = Convert.ToInt64(textArray[2]);
            EarnedMoney = Convert.ToInt64(textArray[3]);
            SpendMoney = Convert.ToInt64(textArray[4]);
            GameVersion = textArray[5];
            InvestmentsInSec = Convert.ToInt64(textArray[6]);
        }
        else
        {
            string statistic = string.Format("{0}/{1}/{2}/{3}/{4}/{5}/{6}", 0, 0, 0, 0,
            0, 0 , 0);
            PlayerPrefs.SetString("Statistic", statistic);
            GetStaticticFromPrefs();
        }
    }

    public void GetInvestmentInsec(long value)
    {
        InvestmentsInSec = value;
    }

    public void IncreaseEarnedMoney(long value)
    {
        EarnedMoney+= value;
    }

    public void IncreaseSpendMoney(long value)
    {
        SpendMoney += value;
    }

    public void IncreaseMailOpened()
    {
        MailsOpenedAmount++;
    }

    public void IncreaseTapAmount()
    {
        TapsAmount++;
    }

    public string GetGameVersion()
    {
        GameVersion = Application.version;
        return GameVersion;
    }

    private void GameTimer()
    {
        double timeSpan = (DateTime.Now - lastTime).TotalSeconds;

        if (timeSpan > time)
        {
            lastTime = DateTime.Now;
            GameTimeAmount++;
        }
    }

    private string ConvertLongToTimeFormat(long amount)
    {
        amount = _gameTimeAmount;

        var sec = amount % 60;
        var min = (amount / 60) % 60;
        var hour = amount / 3600;

        return string.Format("{0,2}:{1,2}:{2,2}", hour.ToString().PadLeft(2, '0'), min.ToString().PadLeft(2, '0'), sec.ToString().PadLeft(2, '0'));
    }
}
