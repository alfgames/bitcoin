﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class FBTracking : MonoBehaviour {
    public static FBTracking Instance;
    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else
        {
            DestroyImmediate(gameObject);
            return;
        }

        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(() => {
                if (FB.IsInitialized)
                {
                    // Signal an app activation App Event
                    FB.ActivateApp();
                    // Continue with Facebook SDK
                    // ...
                    LogGame_startEvent();
                }
                else
                {
                    Debug.Log("Failed to Initialize the Facebook SDK");
                }
            }, (isGameShown) => {
                if (!isGameShown)
                {
                    // Pause the game - we will need to hide
                    Time.timeScale = 0;
                }
                else
                {
                    // Resume the game - we're getting focus again
                    Time.timeScale = 1;
                }
            });
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
            
    }

    private void OnApplicationQuit()
    {
        LogGame_endEvent();
    }

    public void LogGame_startEvent()
    {
        FB.LogAppEvent(
            "game_start"
        );
    }

    public void LogGame_endEvent()
    {
        FB.LogAppEvent(
            "game_end"
        );
    }

    public void LogGame_end_endlessEvent()
    {
        FB.LogAppEvent(
            "game_end_endless"
        );
    }

    public void LogGame_end_levelsEvent()
    {
        FB.LogAppEvent(
            "game_end_levels"
        );
    }

    public void LogAd_shown_interstitialEvent()
    {
        FB.LogAppEvent(
            "ad_shown_interstitial"
        );
    }

    public void LogOmc_shownEvent()
    {
        FB.LogAppEvent(
            "omc_shown"
        );
    }

    public void LogRewarded_shownEvent()
    {
        FB.LogAppEvent(
            "rewarded_shown"
        );
    }
}
