﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class UpdatePCController : MonoBehaviour
{
    public UpdateObjectPC UpdateObjectInstance;
    
    public List<GameObject> UpdatePCUnits;

    [Header("NeedToBuy")]
    public GameObject WhichRequirementsNeedToBuyPanel;
    public Transform RequirementContent;
    public GameObject Requirement;

    [Header("AcceptPanelObjects")]
    public GameObject AcceptToBuyUPCPanel;
    public GameObject NotEnoughBalanceValuePanel;
    public GameObject BonusesPanel;

    public Button AcceptToBuy;

    [Header("Components")]
    public Balance BalanceComponent;
    public Interface InterfaceComponent;

    private readonly string _path = "/Resources/UpdatePCData/UpdatePCData.json";
    private readonly string _defaultPath = "/Resources/UpdatePCData/DefaultUpdatePCData.json";


    private void Awake()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (!File.Exists(Application.persistentDataPath + _path) || !File.Exists(Application.persistentDataPath + _defaultPath))
                StartCoroutine(ChangeLocationOfFIle());
        }

        GetDataFromUpdatePCJsonFile(_path);
    }

    private IEnumerator ChangeLocationOfFIle()
    {
        WWW path = new WWW("");
        byte[] _byteArray;

        switch (Application.platform)
        {
            case RuntimePlatform.Android:
                {
                    path = new WWW("jar:file://" + Application.dataPath + "!/assets/Resources/UpdatePCData/UpdatePCData.json");

                    while (!path.isDone)
                    {
                    }
                    if (!string.IsNullOrEmpty(path.error))
                    {
                        print(path.error);
                    }
                    else
                    {
                        _byteArray = path.bytes;
                        print("_bytearray");

                        var filepath = string.Format("{0}/{1}/{2}/{3}.{4}", Application.persistentDataPath, "Resources",
                            "UpdatePCData", "UpdatePCData", "json");
                        Directory.CreateDirectory(Path.GetDirectoryName(filepath));
                        File.WriteAllBytes(filepath, _byteArray);
                    }


                    path = new WWW("jar:file://" + Application.dataPath + "!/assets/Resources/UpdatePCData/DefaultUpdatePCData.json");

                    while (!path.isDone)
                    {
                    }
                    if (!string.IsNullOrEmpty(path.error))
                    {
                        print(path.error);
                    }
                    else
                    {
                        _byteArray = path.bytes;
                        print("_bytearray");

                        var filepath = string.Format("{0}/{1}/{2}/{3}.{4}", Application.persistentDataPath, "Resources",
                            "UpdatePCData", "DefaultUpdatePCData", "json");
                        Directory.CreateDirectory(Path.GetDirectoryName(filepath));
                        File.WriteAllBytes(filepath, _byteArray);
                    }
                    break;
                }
        }

        yield return null;
    }

    public void SaveDataToUpdatePCJsonFile()
    {
        CreateAndWriteFile(JsonUtility.ToJson(UpdateObjectInstance));
        GetDataFromUpdatePCJsonFile(_path);
    }

    private void CreateAndWriteFile(string json)
    {
        string path = "";
        string folderPath = "/Resources/UpdatePCData/";

        if (Application.platform == RuntimePlatform.Android)
        {
            path = Application.persistentDataPath + folderPath;
        }
        else
        {
            path = Application.dataPath + folderPath;
        }

        byte[] bytesarray = Encoding.UTF8.GetBytes(json);

        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        print(path);

        FileStream stream =
            new FileStream(path + "UpdatePCData.json",
                FileMode.Create);
        stream.Write(bytesarray, 0, bytesarray.Length);
        stream.Close();
    }

    public void GetDataFromUpdatePCJsonFile(string path)
    {
        var contentFromFile = "";

        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            contentFromFile = File.ReadAllText(Application.dataPath + path);
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            contentFromFile = File.ReadAllText(Application.persistentDataPath + path);
        }
        UpdateObjectInstance= JsonUtility.FromJson<UpdateObjectPC>(contentFromFile);
    }


    public void BuyUpdatePCObject(GameObject go)
    {
        print(go.name + " " + UpdateObjectInstance.AllUpdateObject.Find(x => x.ObjectName == go.transform.parent.name).ObjectName);
        var isAlreadyBuy = IsCanBuyUpdatePCObject(go.transform.parent.name);
        var CurrentUpdatePCObject = UpdateObjectInstance.AllUpdateObject.Find(x => x.ObjectName == go.transform.parent.name);
        if (!isAlreadyBuy)
        {
            if (BalanceComponent.BalanceValue >= CurrentUpdatePCObject.ObjectPrice)
            {
                if (CheckToBoughtRequiremetsByObject(CurrentUpdatePCObject))
                {
                    if (GameObject.FindGameObjectWithTag("Sound") != null)
                    {
                        AudioManager.Instance.PlayAudio(AudioManager.Instance.upgradeInvest);
                    }


                    AcceptToBuyUPCPanel.SetActive(true);
                    AcceptToBuy.onClick.RemoveAllListeners();
                    AcceptToBuy.onClick.AddListener(
                        () => BuyAction(CurrentUpdatePCObject.ObjectPrice, CurrentUpdatePCObject));
                }
                else
                {
                    WhichRequirementsNeedToBuyPanel.SetActive(true);
                    ClearRequirementsOnPanel();
                    foreach (var item in UpdatePCUnits)
                    {
                        foreach (var requirement in CurrentUpdatePCObject.Requirements)
                        {
                            if (requirement == item.name)
                            {
                                GameObject req = Instantiate(Requirement, RequirementContent);
                                req.GetComponent<Image>().sprite =
                                    item.transform.Find("UnitImage").GetComponent<Image>().sprite;
                            }
                        }
                    }
                }
            }
            else
            {
                NotEnoughBalanceValuePanel.SetActive(true);
                Button notEnoughBtn = NotEnoughBalanceValuePanel.GetComponentInChildren<Button>();
                notEnoughBtn.onClick.RemoveAllListeners();
                notEnoughBtn.onClick.AddListener(() =>
                {
                    NotEnoughBalanceValuePanel.SetActive(false);
                    BonusesPanel.SetActive(true);
                });
            }
        }
    }

    public void ClearRequirementsOnPanel()
    {
        foreach (Transform item in RequirementContent)
        {
            Destroy(item.gameObject);
        }
    }

    private void BuyAction(long objectPrice, Object curObject)
    {
        BalanceComponent.BalanceValue -= objectPrice;
        StatisticsController.Instance.IncreaseSpendMoney(objectPrice);

        if (curObject.ObjectFixedBonus != 0)
        {
            BalanceComponent.AdditionalValue += curObject.ObjectFixedBonus;
        }

        if (curObject.ObjectFarmingPercent != 0)
        {
            var valueToAdd = (Mathf.RoundToInt((float)BalanceComponent.AdditionalValue * curObject.ObjectFarmingPercent / 100f));
            BalanceComponent.AdditionalValue = BalanceComponent.AdditionalValue + valueToAdd;
        }

        if (curObject.ObjectTapBonus != 0)
        {
            var valueToAdd = (Mathf.RoundToInt((float)BalanceComponent.TaplValue * curObject.ObjectTapBonus / 100f));
            BalanceComponent.TaplValue = BalanceComponent.TaplValue + valueToAdd;
        }
        BalanceComponent.WriteBalanceValues();
        AcceptToBuyUPCPanel.SetActive(false);
        curObject.IsAlreadyBuy = true;
        SaveDataToUpdatePCJsonFile();
        SetAvailableUpdatePCUnits();
    }

    private bool CheckToBoughtRequiremetsByObject(Object obj)
    {
        bool reuslt = true;

        if (obj.Requirements.Count != 0)
        {
            foreach (var requirement in obj.Requirements)
            {
                reuslt = UpdateObjectInstance.AllUpdateObject.Find(a => a.ObjectName == requirement).IsAlreadyBuy;
                if (!reuslt)
                    break;
            }
        }

        return reuslt;
    }

    public bool IsCanBuyUpdatePCObject(string name)
    {
        return UpdateObjectInstance.AllUpdateObject.Find(x => x.ObjectName == name).IsAlreadyBuy;
    }

    public void SetAvailableUpdatePCUnits()
    {
        foreach (var updatePcUnit in UpdatePCUnits)
        {
            updatePcUnit.transform.Find("BuyButton").gameObject.SetActive(!UpdateObjectInstance.AllUpdateObject.Find(y => y.ObjectName == updatePcUnit.name).IsAlreadyBuy);
        }
        UpdateUpgradePCDataUnits();
    }

    public void UpdateUpgradePCDataUnits()
    {
        
        foreach (var updatePcUnit in UpdatePCUnits)
        {
            var curObject = UpdateObjectInstance.AllUpdateObject.Find(y => y.ObjectName == updatePcUnit.name);

            if (curObject.IsAlreadyBuy)
            {
                updatePcUnit.transform.Find("PriceText").GetComponent<Text>().text = "";
            }
            else
            {
                updatePcUnit.transform.Find("PriceText").GetComponent<Text>().text = InterfaceComponent.FormattingValue(curObject.ObjectPrice);
            }
        }
    }

    public void ResetAllUpgradePCDataData()
    {
        #region Read all default data
        var contentFromFile = "";

        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            contentFromFile = File.ReadAllText(Application.dataPath + _defaultPath);
        }
        else
        {
            contentFromFile = File.ReadAllText(Application.persistentDataPath + _defaultPath);
        }
        UpdateObjectInstance = JsonUtility.FromJson<UpdateObjectPC>(contentFromFile);
        #endregion

        #region Write all new data in main file
        CreateAndWriteFile(JsonUtility.ToJson(UpdateObjectInstance));
        GetDataFromUpdatePCJsonFile(_path);
        #endregion
    }

    [Serializable]
    public class UpdateObjectPC
    {
        public List<Object> AllUpdateObject = new List<Object>();
    }

    [Serializable]
    public class Object
    {
        public string ObjectName;
        public long ObjectPrice;
        public int ObjectFarmingPercent;
        public int ObjectTapBonus;
        public long ObjectFixedBonus;
        public bool IsAlreadyBuy;
        public List<string> Requirements = new List<string>(); 
    }
}
