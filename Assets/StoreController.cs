﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class StoreController : MonoBehaviour
{
    public enum StoreItem
    {
        Pet = 0,
        Appearance = 1
    }

    public enum AppearanceItem
    {
        Hair = 0,
        Clothes = 1,
        Glasses = 2
    }

    public enum Charachter
    {
        Null = 0,
        Man = 1,
        Woman = 2
    }

    public enum RoomSkin
    {
        Default = 0,
        HiTech = 1,
    }

    public Charachter PlayerCharachter;

    public ObjectInStore StoreObjectsInstance;

    public List<GameObject> StorePets;
    public List<GameObject> PetsItemsOnScene;

    [Header("AcceptPanelObjects")]
    public GameObject ApplyStoreItemPanel;
    public GameObject NotEnoughBalanceValuePanel;
    public GameObject MonneyExchangePanel;
    public GameObject AcceptToBuyPanel;

    public Button AcceptToBuy;
    public Button ApplyItem;

    [Header("AcceptPanelObjects")]
    public GameObject ChooseCharachterPanel;


    public Sprite[] CharachterBody;

    [Header("PreviewCharachterObjects")]
    public Image PreviewCharachterBody;
    public Image PreviewAqccessoriesImage;
    public Image PreviewHairImage;
    public Image PreviewClothesImage;

    [Header("SceneCharachterObjects")]
    public Image SceneCharachterBody;
    public Image SceneCharachterHair;
    public Image SceneCharachterGlasses;
    public Image SceneCharachterClothes;

    [Header("Store Items Lists")]
    public List<Image> HairItems;
    public List<Image> ClothesItems;
    public List<Image> GlassesItems;

    [Header("Store Man")]
    public List<Sprite> ManHairItemSprites;
    public List<Sprite> ManClothesItemSprites;
    public List<Sprite> ManGlassesItemSprites;
    [Space(5f)]
    public List<Sprite> ManHairSprites;
    public List<Sprite> ManClothesSprites;

    [Header("Store Woman")]
    public List<Sprite> WomanHairItemSprites;
    public List<Sprite> WomanClothesItemSprites;
    public List<Sprite> WomanGlassesItemSprites;
    [Space(5f)]
    public List<Sprite> WomanHairSprites;
    public List<Sprite> WomanClothesSprites;
    [Space(5f)]
    public List<Sprite> GlassesSprites;


    [Header("RoomSkinsUI")]
    public GameObject StaticBG;
    public GameObject[] SceneUI;
    public GameObject Table;
    public GameObject Flowerpot;
    public GameObject Chair;

    public GameObject RoomSkinObject;


    [Header("Components")]
    public Balance BalanceComponent;

    private readonly string _path = "/Resources/StoreData/StoreData.json";
    private readonly string _defaultPath = "/Resources/StoreData/DefaultStoreData.json";

    public GPSManager gpsManager;
    public GameObject tutorialPanel;

    private void Awake()
    {
        print(JsonUtility.ToJson(StoreObjectsInstance));
        if (Application.platform == RuntimePlatform.Android)
        {
            if (!File.Exists(Application.persistentDataPath + _path) || !File.Exists(Application.persistentDataPath + _defaultPath))
                StartCoroutine(ChangeLocationOfFIle());
        }

        GetDataFromStoreJsonFile(_path);

        if (StoreObjectsInstance.AppearanceObject.PlayerCharachter != Charachter.Null)
        {
            InitStoreScene();
        }
        else
        {
            CheckForChoosenCharachter();
        }

        SetAvailablePets();
        SetAvailableRoomSkins();
        SetRoomSkin(StoreObjectsInstance.RoomSkinObject.PlayerRoomSkin);

    }

    private void Start()
    {
        if (PlayerPrefs.GetInt("ShowSeconsTutor") == 1 && PlayerPrefs.GetInt("Tutorial") == 0)
        {
            tutorialPanel.SetActive(true);
        }

    }

    public void InitStoreScene()
    {
            GetCharachterType();
            SetCharachterBody();

        SetUIForAppearanceItem();
        SetUIForPreview();
        SetUIOnScene();
        TutorialManager.Instance.StartSS();
    }

    #region Actions with Files
    private IEnumerator ChangeLocationOfFIle()
    {
        WWW path = new WWW("");
        byte[] _byteArray;

        switch (Application.platform)
        {
            case RuntimePlatform.Android:
                {
                    path = new WWW("jar:file://" + Application.dataPath + "!/assets/Resources/StoreData/StoreData.json");

                    while (!path.isDone)
                    {
                    }
                    if (!string.IsNullOrEmpty(path.error))
                    {
                        print(path.error);
                    }
                    else
                    {
                        _byteArray = path.bytes;
                        print("_bytearray");

                        var filepath = string.Format("{0}/{1}/{2}/{3}.{4}", Application.persistentDataPath, "Resources",
                            "StoreData", "StoreData", "json");
                        Directory.CreateDirectory(Path.GetDirectoryName(filepath));
                        File.WriteAllBytes(filepath, _byteArray);
                    }


                    path = new WWW("jar:file://" + Application.dataPath + "!/assets/Resources/StoreData/DefaultStoreData.json");

                    while (!path.isDone)
                    {
                    }
                    if (!string.IsNullOrEmpty(path.error))
                    {
                        print(path.error);
                    }
                    else
                    {
                        _byteArray = path.bytes;
                        print("_bytearray");

                        var filepath = string.Format("{0}/{1}/{2}/{3}.{4}", Application.persistentDataPath, "Resources",
                            "StoreData", "DefaultStoreData", "json");
                        Directory.CreateDirectory(Path.GetDirectoryName(filepath));
                        File.WriteAllBytes(filepath, _byteArray);
                    }
                    break;
                }
        }

        yield return null;
    }

    public void GetDataFromStoreJsonFile(string path)
    {
        var contentFromFile = "";

        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            contentFromFile = File.ReadAllText(Application.dataPath + path);
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            contentFromFile = File.ReadAllText(Application.persistentDataPath + path);
        }
        StoreObjectsInstance = JsonUtility.FromJson<ObjectInStore>(contentFromFile);
    }

    public void SaveDataToStoreJsonFile()
    {
        CreateAndWriteFile(JsonUtility.ToJson(StoreObjectsInstance));
        GetDataFromStoreJsonFile(_path);
    }

    public void ResetAllStoreData()
    {
        #region Read all default data
        var contentFromFile = "";

        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            contentFromFile = File.ReadAllText(Application.dataPath + _defaultPath);
        }
        else
        {
            contentFromFile = File.ReadAllText(Application.persistentDataPath + _defaultPath);
        }
        StoreObjectsInstance = JsonUtility.FromJson<ObjectInStore>(contentFromFile);
        #endregion

        #region Write all new data in main file
        CreateAndWriteFile(JsonUtility.ToJson(StoreObjectsInstance));
        GetDataFromStoreJsonFile(_path);
        #endregion

        SetAvailableRoomSkins();
        SetRoomSkin(0);

        CheckForChoosenCharachter();
        PlayerPrefs.SetInt("Tutorial", 0);
        PlayerPrefs.SetInt("ShowSeconsTutor", 0);

    }

    private void CreateAndWriteFile(string json)
    {
        string path = "";
        string folderPath = "/Resources/StoreData/";

        if (Application.platform == RuntimePlatform.Android)
        {
            path = Application.persistentDataPath + folderPath;
        }
        else
        {
            path = Application.dataPath + folderPath;
        }

        byte[] bytesarray = Encoding.UTF8.GetBytes(json);

        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        print(path);

        FileStream stream =
            new FileStream(path + "StoreData.json",
                FileMode.Create);
        stream.Write(bytesarray, 0, bytesarray.Length);
        stream.Close();
    }
    #endregion

    #region Appearance logic

    public void CheckForChoosenCharachter()
    {
        ChooseCharachterPanel.SetActive((StoreObjectsInstance.AppearanceObject.PlayerCharachter == Charachter.Null));
    }

    public void ChooseCharachterType(int type)
    {
        PlayerCharachter = (Charachter)type;
        StoreObjectsInstance.AppearanceObject.PlayerCharachter = PlayerCharachter;
        SaveDataToStoreJsonFile();

        ChooseCharachterPanel.SetActive(false);
        if (PlayerPrefs.GetInt("Tutorial") == 0)
        {
            PlayerPrefs.SetInt("ShowSeconsTutor", 1);
            tutorialPanel.SetActive(true);
        }

    }

    public void GetCharachterType()
    {
        PlayerCharachter = StoreObjectsInstance.AppearanceObject.PlayerCharachter;
    }

    public void SetCharachterBody()
    {
        Sprite bodySprite = (PlayerCharachter == Charachter.Man) ? CharachterBody[0] : CharachterBody[1];
        SceneCharachterBody.sprite = bodySprite;
        PreviewCharachterBody.sprite = bodySprite;
    }

    private void SetUIForAppearanceItem()
    {
        for (int i = 0; i < HairItems.Count; i++)
        {
            HairItems[i].sprite = (PlayerCharachter == Charachter.Man) ? ManHairItemSprites[i] : WomanHairItemSprites[i];
        }

        for (int i = 0; i < ClothesItems.Count; i++)
        {
            ClothesItems[i].sprite = (PlayerCharachter == Charachter.Man) ? ManClothesItemSprites[i] : WomanClothesItemSprites[i];
        }

        for (int i = 0; i < GlassesItems.Count; i++)
        {
            GlassesItems[i].sprite = (PlayerCharachter == Charachter.Man) ? ManGlassesItemSprites[i] : WomanGlassesItemSprites[i];
        }
    }

    private void SetUIForPreview()
    {

        string glassesName = "";
        var nameOfGlass =
 StoreObjectsInstance.AppearanceObject.Items.Find(g => g.ItemName.Contains("Glass") && g.IsApply);
                if (nameOfGlass != null)
                    {
            glassesName = Regex.Replace(nameOfGlass.ItemName, "[^0-9 ]", "");
            Sprite appliedGlass = GlassesSprites.Find(gg => gg.name.Contains(glassesName));
                        if ((appliedGlass) ?? appliedGlass)
                           {
                SceneCharachterGlasses.gameObject.SetActive(true);
                PreviewAqccessoriesImage.gameObject.SetActive(true);
                PreviewAqccessoriesImage.sprite = appliedGlass;
                          }
                   }
                else
         {
            PreviewAqccessoriesImage.gameObject.SetActive(false);
            SceneCharachterGlasses.gameObject.SetActive(false);
        }

        string hairName = Regex.Replace(StoreObjectsInstance.AppearanceObject.Items.Find(g => g.ItemName.Contains("Hair") && g.IsApply).ItemName, "[^0-9 ]", "");
        Sprite appliedHair = (PlayerCharachter == Charachter.Man) ? ManHairSprites.Find(gg => gg.name.Contains(hairName)) : WomanHairSprites.Find(gg => gg.name.Contains(hairName));
        if ((appliedHair) ?? appliedHair)
        {
            PreviewHairImage.sprite = appliedHair;
        }
        string clothesName = Regex.Replace(StoreObjectsInstance.AppearanceObject.Items.Find(g => g.ItemName.Contains("Cloth") && g.IsApply).ItemName, "[^0-9 ]", "");
        Sprite appliedCloth = (PlayerCharachter == Charachter.Man) ? ManClothesSprites.Find(gg => gg.name.Contains(clothesName)) : WomanClothesSprites.Find(gg => gg.name.Contains(clothesName));
        if ((appliedCloth) ?? appliedCloth)
        {
            PreviewClothesImage.sprite = appliedCloth;
        }

        SetApplyMarker(clothesName, hairName, glassesName);
    }

    private void SetUIOnScene()
    {
        SceneCharachterHair.sprite = PreviewHairImage.sprite;
        SceneCharachterGlasses.sprite = PreviewAqccessoriesImage.sprite;
        SceneCharachterClothes.sprite = PreviewClothesImage.sprite;
    }

    private void SetApplyMarker(string cloth, string hair, string glass = "")
    {
        foreach (var item in HairItems)
        {
            item.transform.GetChild(0).gameObject.SetActive(item.name == hair);
        }

                if (!string.IsNullOrEmpty(glass))
        {

                       foreach (var item1 in GlassesItems)
                           {
                item1.transform.GetChild(0).gameObject.SetActive(item1.name == glass);
                           }
                    }
              else
        {
            foreach (var item1 in GlassesItems)
            {
                item1.transform.GetChild(0).gameObject.SetActive(false);
            }
        }

            foreach (var item2 in ClothesItems)
        {
            item2.transform.GetChild(0).gameObject.SetActive(item2.name == cloth);
        }
    }

    public void BuyApplyAppearanceItem(GameObject go)
    {
        string itemName = go.transform.parent.name + go.name;
        var isBuy = IsCanBuyAppearance(itemName);
        var CurrentAppearanceObject = StoreObjectsInstance.AppearanceObject.Items.Find(x => x.ItemName == itemName);
        if (!isBuy)
        {
            if (BalanceComponent.BalanceValue >= CurrentAppearanceObject.Price)
            {
                AcceptToBuyPanel.SetActive(true);
                AcceptToBuy.onClick.RemoveAllListeners();
                AcceptToBuy.onClick.AddListener(
                    () => BuyAppearanceAction(CurrentAppearanceObject.Price, CurrentAppearanceObject));
            }
            else
            {
                AcceptToBuyPanel.SetActive(false);
                NotEnoughBalanceValuePanel.SetActive(true);
                Button notEnoughBtn = NotEnoughBalanceValuePanel.GetComponentInChildren<Button>();
                notEnoughBtn.onClick.RemoveAllListeners();
                notEnoughBtn.onClick.AddListener(() =>
                {
                    NotEnoughBalanceValuePanel.SetActive(false);
                });
            }
        }
        else
        {
            if (!CurrentAppearanceObject.IsApply || CurrentAppearanceObject.Type == AppearanceItem.Glasses)
            {
                ApplyAppearanceAction(CurrentAppearanceObject, go.transform.parent.name);
            }
        }
    }

    private void BuyAppearanceAction(long objectPrice, AppearanceObjects curObject)
    {
        BalanceComponent.BalanceValue -= objectPrice;

        AcceptToBuyPanel.SetActive(false);
        curObject.IsBuy = true;
        SaveDataToStoreJsonFile();

        // *** Unlock Achievements ***

        if (PlayerPrefs.GetInt("SKIN") == 0)
        {
            gpsManager.GetAchievement(GPGSIds.achievement_change_skin);
            HardCurrencyManager.hardCurrencyValue += 10;
            PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
            PlayerPrefs.SetInt("SKIN", 1);
        }

        // ***************************
    }

    private void ApplyAppearanceAction(AppearanceObjects item, string type)
    {
        var list = StoreObjectsInstance.AppearanceObject.Items.FindAll(x => x.ItemName.Contains(type));

        foreach (var appear in list)
        {
            if (item.Type != AppearanceItem.Glasses)
                            {
                appear.IsApply = (appear == item);
                           }
                       else
           {
                appear.IsApply = (appear == item) ? !appear.IsApply : false;
                           }
        }

        ApplyStoreItemPanel.SetActive(false);
        SaveDataToStoreJsonFile();

        SetUIForPreview();
        SetUIOnScene();
    }

    public bool IsCanBuyAppearance(string name)
    {
        return StoreObjectsInstance.AppearanceObject.Items.Find(x => x.ItemName == name).IsBuy;
    }
    #endregion

    #region Pets logic

    public void SetAvailablePets()
    {
        foreach (var unit in StorePets)
        {
            Pets pet = StoreObjectsInstance.PetObjects.Find(x => x.Name == unit.name);

            if (pet.IsBuy)
            {
                unit.transform.Find("PriceText").gameObject.SetActive(false);
                unit.transform.Find("ApplyButton").gameObject.SetActive(!StoreObjectsInstance.PetObjects.Find(x => x.Name == unit.name).IsApply);
            }

            unit.transform.Find("BuyButton").gameObject.SetActive(!pet.IsBuy);
        }

        foreach (var item in PetsItemsOnScene)
        {
            Pets petOnScene = StoreObjectsInstance.PetObjects.Find(x => x.Name == item.name);
            item.SetActive(petOnScene.IsApply);
        }
    }

    public void BuyPetsItem(GameObject go)
    {
        var isBuy = IsCanBuyPets(go.name);
        var CurrentPetsObject = StoreObjectsInstance.PetObjects.Find(x => x.Name == go.name);
        if (!isBuy)
        {
            if (HardCurrencyManager.hardCurrencyValue >= CurrentPetsObject.Price)
            {
                AcceptToBuyPanel.SetActive(true);
                AcceptToBuy.onClick.RemoveAllListeners();
                AcceptToBuy.onClick.AddListener(
                    () => BuyPetAction(CurrentPetsObject.Price, CurrentPetsObject));
            }
            else
            {
                AcceptToBuyPanel.SetActive(false);
                NotEnoughBalanceValuePanel.SetActive(true);
                Button notEnoughBtn = NotEnoughBalanceValuePanel.GetComponentInChildren<Button>();
                print(notEnoughBtn.name);

                notEnoughBtn.onClick.RemoveAllListeners();
                notEnoughBtn.onClick.AddListener(() =>
                {
                    NotEnoughBalanceValuePanel.SetActive(false);
                    MonneyExchangePanel.SetActive(true);
                });
            }
        }
    }

    private void BuyPetAction(int objectPrice, Pets curObject)
    {
        HardCurrencyManager.hardCurrencyValue -= objectPrice;

        if (curObject.TapAmount != 0)
        {
            BalanceComponent.TaplValue += curObject.TapAmount;
        }

        AcceptToBuyPanel.SetActive(false);
        curObject.IsBuy = true;
        SaveDataToStoreJsonFile();
        SetAvailablePets();
    }

    public void ApplyPetItem(GameObject go)
    {
        var CurrentPetsObject = StoreObjectsInstance.PetObjects.Find(x => x.Name == go.name);

        ApplyStoreItemPanel.SetActive(true);
        ApplyItem.onClick.RemoveAllListeners();
        ApplyItem.onClick.AddListener(
            () => ApplyPetAction(CurrentPetsObject));
    }

    private void ApplyPetAction(Pets item)
    {
        foreach (var pet in StoreObjectsInstance.PetObjects)
        {
            pet.IsApply = (pet == item);
        }

        ApplyStoreItemPanel.SetActive(false);
        SaveDataToStoreJsonFile();
        SetAvailablePets();
    }

    public void CancelAction(GameObject go)
    {
        go.SetActive(!go.activeSelf);
    }

    public bool IsCanBuyPets(string name)
    {
        return StoreObjectsInstance.PetObjects.Find(x => x.Name == name).IsBuy;
    }

    #endregion

    #region Room Skins

    public void SetRoomSkin(RoomSkin roomSkin)
    {
        if (roomSkin != RoomSkin.Default)
        {
            StaticBG.SetActive(true);
            foreach (var unit in SceneUI)
            {
                if (unit.name != "Image")
                {
                    unit.SetActive(false);
                }
            }

            Table.SetActive(true);
            Flowerpot.SetActive(true);
            Chair.SetActive(true);
        }
        else
        {
            StaticBG.SetActive(false);
            foreach (var unit in SceneUI)
            {
                unit.SetActive(true);
            }
            Table.SetActive(false);
            Flowerpot.SetActive(false);
            Chair.SetActive(false);
        }
    }

    public void BuyRoomSkinAction(int id)
    {
        var isBuy = IsCanBuyRoomSkin(id);
        var CurrentRoomSkinObject = StoreObjectsInstance.RoomSkinObject.RoomSkins.Find(x => (int)x.RoomSkin == id);
        if (!isBuy)
        {
            if (HardCurrencyManager.hardCurrencyValue >= CurrentRoomSkinObject.Price)
            {
                AcceptToBuyPanel.SetActive(true);
                AcceptToBuy.onClick.RemoveAllListeners();
                AcceptToBuy.onClick.AddListener(
                    () => BuyRoomSkinAction(CurrentRoomSkinObject.Price, CurrentRoomSkinObject));
            }
            else
            {
                AcceptToBuyPanel.SetActive(false);
                NotEnoughBalanceValuePanel.SetActive(true);
                Button notEnoughBtn = NotEnoughBalanceValuePanel.GetComponentInChildren<Button>();
                print(notEnoughBtn.name);

                notEnoughBtn.onClick.RemoveAllListeners();
                notEnoughBtn.onClick.AddListener(() =>
                {
                    NotEnoughBalanceValuePanel.SetActive(false);
                    MonneyExchangePanel.SetActive(true);
                });
            }
        }
    }

    private void BuyRoomSkinAction(int objectPrice, RoomSkins curObject)
    {
        HardCurrencyManager.hardCurrencyValue -= objectPrice;

        AcceptToBuyPanel.SetActive(false);
        curObject.IsBuy = true;
        SaveDataToStoreJsonFile();
        SetAvailableRoomSkins();
    }

    public void SetAvailableRoomSkins()
    {
        RoomSkins room = (StoreObjectsInstance.RoomSkinObject.RoomSkins.Find(x => x.IsBuy)) ?? (StoreObjectsInstance.RoomSkinObject.RoomSkins.Find(x => x.IsBuy));

        if (room != null)
        {

            if (room.IsBuy)
            {
                RoomSkinObject.transform.Find("PriceText").gameObject.SetActive(false);
                RoomSkinObject.transform.Find("BonusText").gameObject.SetActive(false);
                RoomSkinObject.transform.Find("ApplyButton")
                    .gameObject.SetActive(
                        !room.IsApply);
            }

            RoomSkinObject.transform.Find("BuyButton").gameObject.SetActive(!room.IsBuy);
        }
        else
        {
           RoomSkinObject.transform.Find("PriceText").gameObject.SetActive(true);
           RoomSkinObject.transform.Find("BonusText").gameObject.SetActive(true);
           RoomSkinObject.transform.Find("BuyButton").gameObject.SetActive(true);
            RoomSkinObject.transform.Find("ApplyButton")
                    .gameObject.SetActive(false);
        }
    }

    public void ApplyRoomSkinAction(int id)
    {
        foreach (var skin in StoreObjectsInstance.RoomSkinObject.RoomSkins)
        {
            skin.IsApply = ((int)skin.RoomSkin == id);
        }
        RoomSkins room = StoreObjectsInstance.RoomSkinObject.RoomSkins.Find(x => (int)x.RoomSkin == id);
        BalanceComponent.AdditionalValue += room.AdditionalValue;

        ApplyStoreItemPanel.SetActive(false);
        StoreObjectsInstance.RoomSkinObject.PlayerRoomSkin = (RoomSkin)id;
        SaveDataToStoreJsonFile();
        SetAvailableRoomSkins();
        SetRoomSkin((RoomSkin)id);
    }

    public bool IsCanBuyRoomSkin(int id)
    {
        return StoreObjectsInstance.RoomSkinObject.RoomSkins.Find(x => (int)x.RoomSkin == id).IsBuy;
    }


    #endregion
    [Serializable]
    public class ObjectInStore
    {
        public List<Pets> PetObjects = new List<Pets>();
        public Appearance AppearanceObject;
        public Room RoomSkinObject;
    }

    [Serializable]
    public class Pets
    {
        public StoreItem Item;
        public string Name;
        public int Price;
        public int TapAmount;
        public bool IsApply;
        public bool IsBuy;
    }

    [Serializable]
    public class Appearance
    {
        public Charachter PlayerCharachter;
        public List<AppearanceObjects> Items;
    }


    [Serializable]
    public class Room
    {
        public RoomSkin PlayerRoomSkin;
        public List<RoomSkins> RoomSkins = new List<RoomSkins>();
    }

    [Serializable]
    public class RoomSkins
    {
        public RoomSkin RoomSkin;
        public bool IsBuy;
        public bool IsApply;
        public int Price;
        public long AdditionalValue;
    }


    [Serializable]
    public class AppearanceObjects
    {
        public AppearanceItem Type;
        public string ItemName;
        public long Price;
        public bool IsBuy;
        public bool IsApply;
    }
}


