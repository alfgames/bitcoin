﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public static AudioManager Instance { get; private set; }

    public AudioClip mailCreate;
    public AudioClip mailOpen;
    public AudioClip okSound;
    public AudioClip upgradeInvest;
    public AudioClip upgradeRoomsItem;
    public AudioClip tapSound;

    public AudioClip bonusCoinClick;
    public AudioClip bonusVirusClick;
    public AudioClip bonusGameOver;


    public AudioSource audioSource;

    private void Awake()
    {
        Instance = this;
    }


    public void PlayAudio (AudioClip clip) {

        audioSource.PlayOneShot(clip);

    }
}
