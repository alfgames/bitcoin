﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BonusesController : MonoBehaviour
{
    public MailsController MailsControllerComponent;
    public Balance BalanceComponent;

    public GameObject AcceptToBuyPanel;
    public Button AcceptToBuy;

    public GameObject NotEnoughBalanceValuePanel;
    public GameObject MonneyExchangePanel;

    public GameObject InvestmentMultiplyButton;
    public GameObject InvestmentMultiplyPrice;

    private void Start()
    {
        if (PlayerPrefs.HasKey("InvestmentMultiply"))
        {
            InvestmentMultiplyPrice.SetActive(false);
            InvestmentMultiplyButton.SetActive(false);
        }
    }

    public void OnAutominerBonusClick()
    {
        BuyBonussesIfMay(delegate { StartCoroutine(MailsControllerComponent.BonusXTap(15, true, false, 30f)); }, 10);
        print("OnAutominerBonusClick");

    }

    public void OnBitcoinConverterBonusClick()
    {
        var additionalValue = MailsControllerComponent.BalanceComponent.AdditionalValue;
        BuyBonussesIfMay(delegate { MailsControllerComponent.PlusBalance(additionalValue, 50); }, 15);
        print("OnBitcoinConverterBonusClick");

    }

    public void OnExplosionBonusClick()
    {
        BuyBonussesIfMay(delegate { StartCoroutine(MailsControllerComponent.IncreaseInvestments(true, 30f)); },50);
        print("OnExplosionBonusClick");
    }

    public void OnCrazyBonusClick()
    {
        BuyBonussesIfMay(delegate { StartCoroutine(MailsControllerComponent.BonusXTap(50, true, true, 30f)); }, 50);
        print("OnCrazyBonusClick");

    }

    public void OnBitcoinGoldBonusClick()
    {
        BuyBonussesIfMay(delegate {
            PlayerPrefs.SetInt("InvestmentMultiply", 2);
            BalanceComponent.NewAdditionalIncome();
            AcceptToBuyPanel.SetActive(false);
            InvestmentMultiplyPrice.SetActive(false);
            InvestmentMultiplyButton.SetActive(false);
        }, 150);
        print("OnBitcoinGoldBonusClick");
    }

    private void BuyBonussesIfMay(Action action, int price)
    {
        if (HardCurrencyManager.hardCurrencyValue >= price)
        {
            AcceptToBuyPanel.SetActive(true);
            AcceptToBuy.onClick.RemoveAllListeners();
            AcceptToBuy.onClick.AddListener(
                    delegate
                    {
                        HardCurrencyManager.hardCurrencyValue -= price;
                        action();
                        AcceptToBuyPanel.SetActive(false);
                    });

        }
        else
        {
            NotEnoughBalanceValuePanel.SetActive(true);
            Button notEnoughBtn = NotEnoughBalanceValuePanel.GetComponentInChildren<Button>();
            print(notEnoughBtn.name);

            notEnoughBtn.onClick.RemoveAllListeners();
            notEnoughBtn.onClick.AddListener(() =>
            {
                AcceptToBuyPanel.SetActive(false);
                NotEnoughBalanceValuePanel.SetActive(false);
                MonneyExchangePanel.SetActive(true);
            });
        }
    }
}
