﻿using UnityEngine;
using System.Collections;

public class Click : MonoBehaviour {

    public UnityEngine.UI.Text fpc;
    public UnityEngine.UI.Text fundsDisplay;
    public float funds = 0;
    public int fundsPerClick = 1;

    void Update()
    {
        //if (CurrencyConverter.Instance == null) return;
        fundsDisplay.text = "" + CurrencyConverter.Instance.GetCurrencyIntoString(funds, false, false);
        fpc.text = CurrencyConverter.Instance.GetCurrencyIntoString(fundsPerClick, false, true) + " /Click";
    }

    public void Clicked(bool wSound = true)
    {
        funds += fundsPerClick;

        if (wSound && GameObject.FindGameObjectWithTag("Sound") != null)
        {
            AudioManager.Instance.PlayAudio(AudioManager.Instance.tapSound);
        }

    }

}
