﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.IO;

public class Database : MonoBehaviour
{
    public GameObject gameHelper;
    public Interface _interface;
    public Balance balance;

    public List<Investment> investmentCatalog;
    public List<myInvestment> myInvestments;
    public List<string> InvestmentType;
    public List<InvestmentTile> investmentTiles;

    public int maxInvestmentLevel = 50;
    public int maxInvestments = 10;

    private readonly string InvUpgradeFilePath = "/Informations/Investment Upgrade.txt";
    private readonly string InvLevelBonusFilePath = "/Informations/Investment Level Bonus.txt";
    private readonly string MyInvestmentFilePath = "/Informations/My Investment.txt";
    private readonly string InvTypeFilePath = "/Informations/Investment Type.txt";

    void Awake()
    {
        ChangeFileLocation();
        ReadInformationFiles();
        ReadMyInvestments();
        CreateInvestmentsTile();
    }



    private void ReadInformationFiles()
    {
        ReadInvestmentType();
        ReadUpgradeFile();
        ReadLevelBonusFile();
    }

    public TextAsset textInvestmentLevel, textInvestmentType, textInvestmentUpgrade, textMyInvestment;
    private void ChangeFileLocation()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (!File.Exists(Application.persistentDataPath + InvUpgradeFilePath)
                || !File.Exists(Application.persistentDataPath + InvLevelBonusFilePath)
                || !File.Exists(Application.persistentDataPath + InvLevelBonusFilePath)
                || !File.Exists(Application.persistentDataPath + InvLevelBonusFilePath))
                StartCoroutine(ChangeLocationOfFIle());
        }
    }


    private IEnumerator ChangeLocationOfFIle()
    {
        var filepath = string.Format("{0}/{1}/{2}.{3}", Application.persistentDataPath, "Informations",
                 "Investment Upgrade", "txt");
        Directory.CreateDirectory(Path.GetDirectoryName(filepath));
        File.WriteAllBytes(filepath, textInvestmentUpgrade.bytes);

        filepath = string.Format("{0}/{1}/{2}.{3}", Application.persistentDataPath, "Informations",
                "Investment Level Bonus", "txt");
        Directory.CreateDirectory(Path.GetDirectoryName(filepath));
        File.WriteAllBytes(filepath, textInvestmentLevel.bytes);

        filepath = string.Format("{0}/{1}/{2}.{3}", Application.persistentDataPath, "Informations",
              "My Investment", "txt");
        Directory.CreateDirectory(Path.GetDirectoryName(filepath));
        File.WriteAllBytes(filepath, textMyInvestment.bytes);

        filepath = string.Format("{0}/{1}/{2}.{3}", Application.persistentDataPath, "Informations",
                "Investment Type", "txt");
        Directory.CreateDirectory(Path.GetDirectoryName(filepath));
        File.WriteAllBytes(filepath, textInvestmentType.bytes);

        yield break;
    }

    private void ReadUpgradeFile()
    {
        StreamReader fs;
        if (Application.platform == RuntimePlatform.Android)
        {
            fs = new StreamReader(Application.persistentDataPath + InvUpgradeFilePath);

        }
        else
        {
            fs = new StreamReader(Application.dataPath + @"\Informations\Investment Upgrade.txt");
        }
        string text = fs.ReadToEnd();
        string[] stringSeparators = new string[] { "\r\n", "\n" };
        string[] textArray = text.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
        int semicolonIndex;
        investmentCatalog = new List<Investment>();

        for (int i = 0; i < maxInvestments; i++)
        {
            investmentCatalog.Add(new Investment(InvestmentType[i],
                                                  new List<InvestmentUpgrade>(),
                                                  new List<InvestmentLevelBonus>()));
            int level = 0;

            do
            {
                semicolonIndex = textArray[i].IndexOf(";");
                string tempString = textArray[i];
                long cost = Convert.ToInt64(tempString.Remove(semicolonIndex));

                investmentCatalog[i].upgradeCost.Add(new InvestmentUpgrade(level, cost));

                textArray[i] = textArray[i].Substring(semicolonIndex + 1);
                level++;
            }
            while (level != maxInvestmentLevel);
        }

        fs.Close();
    }

    private void ReadLevelBonusFile()
    {
        StreamReader fs;
        if (Application.platform == RuntimePlatform.Android)
        {
            fs = new StreamReader(Application.persistentDataPath + InvLevelBonusFilePath);

        }
        else
        {
            fs = new StreamReader(Application.dataPath + @"\Informations\Investment Level Bonus.txt");
        }
        string text = fs.ReadToEnd();
        string[] stringSeparators = new string[] { "\r\n", "\n" };
        string[] textArray = text.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
        int semicolonIndex;

        for (int i = 0; i < maxInvestments; i++)
        {
            int level = 0;

            do
            {
                semicolonIndex = textArray[i].IndexOf(";");
                string tempString = textArray[i];
                long bonus = Convert.ToInt64(tempString.Remove(semicolonIndex));

                investmentCatalog[i].levelBonus.Add(new InvestmentLevelBonus(level, bonus));

                textArray[i] = textArray[i].Substring(semicolonIndex + 1);
                level++;
            }
            while (level != maxInvestmentLevel + 1);
        }

        fs.Close();
    }

    private void ReadMyInvestments()
    {
        StreamReader fs;
        if (Application.platform == RuntimePlatform.Android)
        {
            fs = new StreamReader(Application.persistentDataPath + MyInvestmentFilePath);

        }
        else
        {
            fs = new StreamReader(Application.dataPath + @"\Informations\My Investment.txt");
        }
        string text = fs.ReadToEnd();
        string[] stringSeparators = new string[] { "\r\n", "\n" };
        string[] textArray = text.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
        int semicolonIndex;

        myInvestments = new List<myInvestment>();
        for (int i = 0; i < maxInvestments; i++)
        {
            semicolonIndex = textArray[i].IndexOf(";");
            string tempString = textArray[i];
            int level = Convert.ToInt32(tempString.Remove(semicolonIndex));

            textArray[i] = textArray[i].Substring(semicolonIndex + 1);
            tempString = textArray[i];

            int bonus = Convert.ToInt32(tempString.Substring(0, tempString.Length - 1));
            myInvestments.Add(new myInvestment(InvestmentType[i], level, bonus));
        }
        fs.Close();

        balance.NewAdditionalIncome();
    }

    private void WriteMyInvestments(string type, int level, long bonus)
    {
        StreamReader fs;
        if (Application.platform == RuntimePlatform.Android)
        {
            fs = new StreamReader(Application.persistentDataPath + MyInvestmentFilePath);

        }
        else
        {
            fs = new StreamReader(Application.dataPath + @"\Informations\My Investment.txt");
        }
        string text = fs.ReadToEnd();
        string[] stringSeparators = new string[] { "\r\n", "\n" };
        string[] textArray = text.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
        int index = GetTypeIndexByName(type);

        textArray[index] = level.ToString() + ";" + bonus.ToString() + ";";
        fs.Close();

        StreamWriter outputFile;
        if (Application.platform == RuntimePlatform.Android)
        {
            outputFile = new StreamWriter(Application.persistentDataPath + MyInvestmentFilePath);

        }
        else
        {
            outputFile = new StreamWriter(Application.dataPath + @"\Informations\My Investment.txt");
        }
        foreach (string line in textArray)
        {
            outputFile.WriteLine(line);
        }
        outputFile.Close();

        //ADDING TO STATICTICS
        long allInvestmentInSec = 0;
        int semicolonIndex;

        for (int i = 0; i < maxInvestments; i++)
        {
            semicolonIndex = textArray[i].IndexOf(";");
            string tempString = textArray[i];
            textArray[i] = textArray[i].Substring(semicolonIndex + 1);
            tempString = textArray[i];

            int inves = Convert.ToInt32(tempString.Substring(0, tempString.Length - 1));
            allInvestmentInSec += inves;
        }
        StatisticsController.Instance.GetInvestmentInsec(allInvestmentInSec);
    }

    private void ReadInvestmentType()
    {
        Encoding enc = Encoding.GetEncoding(65001);
        //Encoding enc = Encoding.GetEncoding(1251);
        StreamReader fs;
        if (Application.platform == RuntimePlatform.Android)
        {
            fs = new StreamReader(Application.persistentDataPath + InvTypeFilePath, enc);

        }
        else
        {
            fs = new StreamReader(Application.dataPath + @"\Informations\Investment Type.txt", enc);
        }
        string text = fs.ReadToEnd();
        string[] stringSeparators = new string[] { "\r\n", "\n" };
        string[] textArray = text.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

        InvestmentType = new List<string>();

        for (int i = 0; i < maxInvestments; i++)
        {
            InvestmentType.Add(textArray[i]);
        }

        fs.Close();
    }

    private void CreateInvestmentsTile()
    {
        investmentTiles = new List<InvestmentTile>();

        for (int i = 0; i < investmentCatalog.Count; i++)
        {
            GameObject investmentObject = Instantiate(_interface.investmentPrefab, _interface.investmnetsBoard.transform) as GameObject;
            investmentTiles.Add(new InvestmentTile(investmentObject.transform.Find("name").GetComponent<Text>(),
                investmentObject.transform.Find("LevelPanel").Find("Level").GetComponent<Text>(),
                investmentObject.transform.Find("PricePanel").Find("Price").GetComponent<Text>(),
                investmentObject.transform.Find("ValuePanel").Find("Value").GetComponent<Text>(),
                investmentObject.transform.Find("EarningPanel").Find("Earning").GetComponent<Text>(),
                investmentObject.transform.Find("Image").GetComponent<Image>()));

            int number = i;
            investmentObject.transform.Find("Invest").GetComponent<InvestmentButton>().investmentUpdateNumber = number;

            number++;
            string path = "";

            path = "Images/TabletUI/Investments/UI_tablet_window_investments_b_icons_";

            if (number < 10)
            {
                path += "0";

            }
            path += number;
            Sprite sprite = (Sprite)Resources.Load(path, typeof(Sprite));
            investmentObject.transform.Find("Image").GetComponent<Image>().sprite = sprite;
        }
    }



    public int GetTypeIndexByName(string name)
    {
        for (int i = 0; i < InvestmentType.Count; i++)
        {
            if (InvestmentType[i] == name) return i;
        }

        return 0;
    }

    public int GetMyInvestmentIndex(string type)
    {
        for (int i = 0; i < myInvestments.Count; i++)
        {
            if (myInvestments[i].type == type) return i;
        }

        return 0;
    }

    public int GetInvestmentIndexInCatalog(string type)
    {
        for (int i = 0; i < investmentCatalog.Count; i++)
        {
            if (investmentCatalog[i].type == type) return i;
        }

        return 0;
    }

    public long GetInvestmentUpgradeCost(string type, int level)
    {
        for (int i = 0; i < investmentCatalog.Count; i++)
        {
            if (investmentCatalog[i].type == type)
            {
                for (int j = 0; j < investmentCatalog[i].upgradeCost.Count; j++)
                {
                    if (investmentCatalog[i].upgradeCost[j].level == level)
                    {
                        return investmentCatalog[i].upgradeCost[j].cost;
                    }
                }

            }
        }

        return 0;
    }

    public long GetInvestmentLevelBonus(string type, int level)
    {
        long result = 0;
        int index = GetInvestmentIndexInCatalog(type);
        for (int i = 0; i <= level; i++)
        {
            result += investmentCatalog[index].levelBonus[i].bonusIncome;
        }

        return result;
    }

    public int GetInvestmentLevel(string type)
    {
        for (int i = 0; i < myInvestments.Count; i++)
        {
            if (myInvestments[i].type == type) return myInvestments[i].level;
        }

        return -1;
    }

    public string GetInvestmentTypeByIndex(int index)
    {
        for (int i = 0; i < investmentCatalog.Count; i++)
        {
            if (i == index) return investmentCatalog[i].type;
        }

        return "";
    }

    public void UpgradeInvestment(string type)
    {
        int index = GetMyInvestmentIndex(type);
        int indexInCatalog = GetInvestmentIndexInCatalog(type);

        int newLevel = myInvestments[index].level + 1;
        long newBonus = GetInvestmentLevelBonus(type, newLevel);

        myInvestment newInvestment = new myInvestment(type, newLevel, newBonus);

        myInvestments[index] = newInvestment;

        WriteMyInvestments(type, newLevel, newBonus);
    }

    public void ZeroMyInvestments()
    {
        for (int i = 0; i < maxInvestmentLevel; i++)
        {
            string type = GetInvestmentTypeByIndex(i);
            WriteMyInvestments(type, 0, 0);
        }

        ReadMyInvestments();
    }




}
