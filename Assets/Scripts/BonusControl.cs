﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusControl : MonoBehaviour {

    public Balance balance;
    public MailsController mailController;


    //--вытягивать из игры
    private int HARD;
    private int INVEST = 100;
    //------------------

    //----Бонусы---
    private int HARD_BONUS;

    private int INVEST_NOW_COEFFICIENT;
    private int INVEST_NORMAL_COEFFICIENT;
    private int INVEST_BONUS_1;
    private int INVEST_BONUS_2;
    private int INVEST_BONUS_TIME;//сек

    private int TAP_NOW_COEFFICIENT;
    private int TAP_NORMAL_COEFFICIENT;
    private int TAP_BONUS_2;
    private int TAP_BONUS_3;
    private int TAP_BONUS_TIME; //сек

    private int MONEY_BONUS_1;
    private int MONEY_BONUS_2;
    private int MONEY_BONUS_3;
    //------------
    public GameObject panelBonusName;
    public Text textBonusName;
    public Button buttonOkBonusName;
    //------------
    public string textBonusHard;
    public string textBonusInvest;
    public string textBonusTab;
    public string textBonusMoney;

    float WAIT_SECOND_BEFORE_SHOW_NAME_BONUS = 0.5f;

    public Text textStats;

    void Start ()
    {
        HARD = PlayerPrefs.GetInt("HardCurrencyValue");
        HARD_BONUS = 4;

        INVEST_NORMAL_COEFFICIENT = 1;
        INVEST_NOW_COEFFICIENT = INVEST_NORMAL_COEFFICIENT;
        INVEST_BONUS_1 = INVEST_NOW_COEFFICIENT * 2;
        INVEST_BONUS_2 = INVEST_NOW_COEFFICIENT * 4;
        INVEST_BONUS_TIME = 30;//сек

        TAP_NORMAL_COEFFICIENT = 1;
        TAP_NOW_COEFFICIENT = TAP_NORMAL_COEFFICIENT;
        TAP_BONUS_2 = TAP_NOW_COEFFICIENT * 2;
        TAP_BONUS_3 = TAP_NOW_COEFFICIENT * 4;
        TAP_BONUS_TIME = 30; //сек

        MONEY_BONUS_1 = 1000;
        MONEY_BONUS_2 = 50000;
        MONEY_BONUS_3 = 1000000;

        panelBonusName.SetActive(false);
    }


    public void ActivateBonus(string nameBonus)
    {
        Debug.Log("nameBonus: " + nameBonus);
        switch (nameBonus)
        {
            case "casino_icons_hard_m":
                // Hard currency bonus
                StartCoroutine(WaitBeforeShowWindowBonusName(textBonusHard + HARD_BONUS.ToString()));
                HardCurrencyManager.hardCurrencyValue -= HARD_BONUS;
                PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
                break;
            case "casino_icons_invest_bonus_01":
                // Investmance bonus 1 
                StartCoroutine(mailController.BonusXAddValue(INVEST_BONUS_1, true, true, INVEST_BONUS_TIME));
                StartCoroutine(WaitBeforeShowWindowBonusName("Bonus x2 to investments for 30 seconds!"));
                //INVEST_NOW_COEFFICIENT = INVEST_BONUS_1;
                //StartCoroutine(StartBonusInvest(INVEST_BONUS_TIME));
                break;
            case "casino_icons_invest_bonus_02":
                // Investmance bonus 2
                StartCoroutine(mailController.BonusXAddValue(INVEST_BONUS_1, true, true, INVEST_BONUS_TIME));

                StartCoroutine(WaitBeforeShowWindowBonusName("Bonus x4 to investments for 30 seconds!"));
                //INVEST_NOW_COEFFICIENT = INVEST_BONUS_2;
                //StartCoroutine(StartBonusInvest(INVEST_BONUS_TIME));
                break;
            case "casino_icons_invest_tap_02":
                // Tap bonus 1
                StartCoroutine(mailController.BonusXTap(TAP_BONUS_2, true, true, 30f));
                StartCoroutine(WaitBeforeShowWindowBonusName("Bonus x2 to tap for 30 seconds!"));
                //TAP_NOW_COEFFICIENT = TAP_BONUS_2;
                //StartCoroutine(StartBonusTap(TAP_BONUS_TIME));
                break;
            case "casino_icons_invest_tap_03":
                // Tap bonus 2
                StartCoroutine(mailController.BonusXTap(TAP_BONUS_3, true, true, 30f));
                StartCoroutine(WaitBeforeShowWindowBonusName("Bonus x4 to tap for 30 seconds!"));
                //TAP_NOW_COEFFICIENT = TAP_BONUS_3;
                //StartCoroutine(StartBonusTap(TAP_BONUS_TIME));
                break;
            case "casino_icons_money_01":
                // Money bonus 1
                StartCoroutine(WaitBeforeShowWindowBonusName("Jackpot! You won 1 Kb!"));
                balance.BalanceValue += MONEY_BONUS_1;
                break;
            case "casino_icons_money_02":
                // Money bonus 2
                StartCoroutine(WaitBeforeShowWindowBonusName("Jackpot! You won 50 Kb!"));
                balance.BalanceValue += MONEY_BONUS_2;
                break;
            case "casino_icons_money_03":
                // Money bonus 3
                StartCoroutine(WaitBeforeShowWindowBonusName("Jackpot! You won 1 Mb!"));
                balance.BalanceValue += MONEY_BONUS_3;
                break;

        }
    }

    IEnumerator StartBonusInvest( int time)
    {
        yield return new WaitForSeconds(time);
        INVEST_NOW_COEFFICIENT = INVEST_NORMAL_COEFFICIENT;
    }

    IEnumerator StartBonusTap(int time)
    {
        yield return new WaitForSeconds(time);
        TAP_NOW_COEFFICIENT = TAP_NORMAL_COEFFICIENT;
    }

    IEnumerator WaitBeforeShowWindowBonusName(string text)
    {
        yield return new WaitForSeconds(WAIT_SECOND_BEFORE_SHOW_NAME_BONUS);
        panelBonusName.SetActive(true);
        textBonusName.text = text;
    }

    public void ButtonOkForBonusName()
    {
        panelBonusName.SetActive(false);
    }
}