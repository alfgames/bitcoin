﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour {
    public static TutorialManager Instance;
    public GameObject tutorialWin;
    public GameObject[] bgImage;
    public Text tutorialText;

    private int textCounter;

    public GameObject[] mailAndTablet;
    public GameObject acceptPanel;

    public GameObject blockedImage;
    public GameObject cataclism;
    public GameObject clickMode;

    private string[] currentText = 
    {
        /*
        "Welcome to the Bitcoins World!\n\nLet me tell you, what's to be done!", // Start text
        "In order to start farming bitcoins, click here!", // Click mode text
        "Tap on the screen with your finger to get bitcoins.\nThe faster you tap, the more you get!", // Click mode text 2
        "For now, enough, let's get out of this mode!", // Cansel click mode text
        */
        "Now you have enough money to invest your money the first project. Buy investment and upgrade the PC to get more bitcoins per second.", // Invest text
 
        "Also you can change the elements of the room. To do this, just click on them.", // Upgrade room items text
        "Also you can change the appearance of the character, buy pets and the skin of the whole room. To do this, go to the shop!", // Bonuses text
        "Beware of unforeseen situations! Fires and earthquakes are not uncommon, and if you do not react in time - you risk losing money! To get rid of them, just tap for every fire or rain 3 times.", // Cataclysm text
        "Also you can earn bitcoins and bonus game or try your luck at the casino. And read the mails! Good luck!", // Bonus game/casino text
        "Also you can earn bitcoins and bonus game or try your luck at the casino. And read the mails! Good luck!" // Bonus game/casino text

    };

    private void Awake()
    {
        Instance = this;
    }

    public void StartSS()
    {
        textCounter = 0;
        if (PlayerPrefs.GetInt("Tutorial") == 0)
        {
            mailAndTablet[0].SetActive(false);
            mailAndTablet[1].SetActive(false);
            cataclism.SetActive(false);
            //bgImage[0].SetActive(true);
            //ShowTutorPanel(0);
        }
        else if (PlayerPrefs.GetInt("Tutorial") == 1)
        {
            mailAndTablet[0].SetActive(true);
            mailAndTablet[1].SetActive(true);
            cataclism.SetActive(true);
        }
    }
	
	void Update () {
        if (bgImage[3].activeInHierarchy)
        {
            mailAndTablet[0].SetActive(true);
            mailAndTablet[1].SetActive(true);
        }
        if (acceptPanel.activeInHierarchy)
        {
            bgImage[6].SetActive(true);
        }
    }

    public void OkBtn()
    {
        textCounter++;
        tutorialText.text = currentText[textCounter].ToString();
        if (textCounter > 4)
        {
            tutorialWin.SetActive(false);
            PlayerPrefs.SetInt("Tutorial", 1);
            PlayerPrefs.Save();
        }

    }

    public void CloseTutorial()
    {
        tutorialWin.SetActive(false);
    }

    public void CloseTutorPanel()
    {
        if (PlayerPrefs.GetInt("Tutorial") == 0)
        {
            bgImage[1].SetActive(false);
            //blockedImage.SetActive(true);
            ClickTutor();
            //Invoke("ClickTutor", 0.5f);
        }
    }

    public void ClickTutor()
    {
        blockedImage.SetActive(false);
        clickMode.SetActive(false);
        bgImage[2].SetActive(true);

    }

    public void ShowTutorPanel(int i)
    {
        if (PlayerPrefs.GetInt("Tutorial") == 0)
        {
            bgImage[i - 1].SetActive(false);
            bgImage[i].SetActive(true);
        }
    }

    public void CloseTutorPanel2()
    {
        if (PlayerPrefs.GetInt("Tutorial") == 0)
        {
            bgImage[8].SetActive(false);
            ClickTutor2();
            //Invoke("ClickTutor2", 0.5f);
        }
    }
    public void ClickTutor2()
    {
        
        clickMode.SetActive(true);
        bgImage[9].SetActive(true);
        cataclism.SetActive(true);
    }
}
