﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Purchasing;

public class DemoBilling : MonoBehaviour, IStoreListener
{
    private static IStoreController m_StoreController;
    private static IExtensionProvider m_StoreExtensionProvider;
    [Multiline]
    public string PublicKey = "Public key Google Play";

    public List<InforProducts> infomationProducts;
    //public static Purchaser Instance;

    //void Awake()
    //{
    //    Instance = this;
    //}

    void Start()
    {
        // If we haven't set up the Unity Purchasing reference
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }
    }

    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        List<string> lstStore = new List<string>();
        lstStore.Add(Store.GooglePlay.ToString());
        lstStore.Add(Store.AppleAppStore.ToString());
        lstStore.Add(Store.WinRT.ToString());
        lstStore.Add(Store.AmazonApps.ToString());
        lstStore.Add(Store.TizenStore.ToString());
        lstStore.Add(Store.SamsungApps.ToString());
        lstStore.Add(Store.FacebookStore.ToString());
        lstStore.Add(Store.XiaomiMiPay.ToString());
        lstStore.Add(Store.MoolahAppStore.ToString());
        lstStore.Add(Store.MacAppStore.ToString());

        //add product
        foreach (var item in infomationProducts)
        {
            if (item.ID != "")
            {
                IDs id = new IDs();
                if (item.DetailProductID != null && item.DetailProductID.Count != 0)
                {
                    List<string> copyStore = new List<string>();
                    copyStore = lstStore;
                    foreach (var store in item.DetailProductID)
                    {
                        id.Add(store.ID, store.Store.ToString());
                        for (int i = 0; i < copyStore.Count; i++)
                            if (copyStore[i].ToString() == store.Store.ToString())
                                copyStore[i] = "";
                    }

                    foreach (var it in copyStore)
                    {
                        if (it != "")
                            id.Add(item.ID, it.ToString());
                    }
                }
                else
                {
                    id.Add(item.ID, Store.GooglePlay.ToString());
                    id.Add(item.ID, Store.AppleAppStore.ToString());
                    id.Add(item.ID, Store.WinRT.ToString());
                    id.Add(item.ID, Store.AmazonApps.ToString());
                    id.Add(item.ID, Store.TizenStore.ToString());
                    id.Add(item.ID, Store.SamsungApps.ToString());
                    id.Add(item.ID, Store.FacebookStore.ToString());
                    id.Add(item.ID, Store.XiaomiMiPay.ToString());
                    id.Add(item.ID, Store.MoolahAppStore.ToString());
                    id.Add(item.ID, Store.MacAppStore.ToString());
                }
                builder.AddProduct(item.ID, item.ProductType, id);
            }
        }
        builder.Configure<IGooglePlayConfiguration>().SetPublicKey(PublicKey);
        UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void BuyProductID(string productId)
    {
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);

            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);
            }
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    public string LocalizedPrice(string ID)
    {
        if (m_StoreController != null)
            return m_StoreController.products.WithID(ID).metadata.localizedPriceString;
        else return "";
    }

    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            Debug.Log("RestorePurchases started ...");

            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) =>
            {
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        else
        {
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: PASS");
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
            switch (args.purchasedProduct.definition.id)
            {
                case pack1:
                    HardCurrencyManager.hardCurrencyValue += 50;
                    PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
                    tracker.PurchaseTracking("1.99 $");
                    break;
                case pack2:
                    HardCurrencyManager.hardCurrencyValue += 150;
                    PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
                    tracker.PurchaseTracking("4.99 $");
                    break;
                case pack3:
                    HardCurrencyManager.hardCurrencyValue += 350;
                    PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
                    tracker.PurchaseTracking("9.99 $");
                    break;
                case pack4:
                    HardCurrencyManager.hardCurrencyValue += 1000;
                    PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
                    tracker.PurchaseTracking("24.99 $");
                    break;
                case pack5:
                    HardCurrencyManager.hardCurrencyValue += 2000;
                    PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
                    tracker.PurchaseTracking("49.99 $");
                    break;
                case pack6:
                    HardCurrencyManager.hardCurrencyValue += 5000;
                    PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
                    tracker.PurchaseTracking("99.99 $");
                    break;
            }
        return PurchaseProcessingResult.Complete;
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
    public AppsFlyerManager tracker;
	public const string pack1 = "pack_1";
    public const string pack2 = "pack_2";
    public const string pack3 = "pack_3";
    public const string pack4 = "pack_4";
    public const string pack5 = "pack_5";
    public const string pack6 = "pack_6";

    public void BuyBtn(string sku)
    {
        BuyProductID(sku);
    }
}


[System.Serializable]
public class InforProducts
{
    public string Name;
    [Tooltip("If the empty data field is understood to be advertising")]
    public string ID;
    [Tooltip("Add all ID products if you want to customize the store. Emptying will initialize all stores and using default ID")]
    public List<DetailProdutcs> DetailProductID;
    public Sprite Sprite;
    public ProductType ProductType;
    [Multiline]
    public string Description;
    [Tooltip("Default price. Currency unit in USA dolla")]
    public float Price;
    public Modification Modification;
    public int Quantity;
}

[System.Serializable]
public class DetailProdutcs
{
    public Store Store;
    public string ID;
}

[System.Serializable]
public enum Modification
{
    None,
    Hint,
    Coin,
    EXP
}

public enum Store
{
    GooglePlay,
    AppleAppStore,
    MacAppStore,
    WinRT,
    AmazonApps,
    TizenStore,
    SamsungApps,
    FacebookStore,
    XiaomiMiPay,
    MoolahAppStore
}