﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class PlayerDataController : MonoBehaviour
{
    public static PlayerDataController Instance;

    public Player PlayerProgress;

    private readonly string _path = "/Resources/PlayerData/PlayerData.json";
    private readonly string _defaultPath = "/Resources/PlayerData/DefaultData.json";


    private void Awake()
    {
        Instance = this;
        if (Application.platform == RuntimePlatform.Android)
        {
            if (!File.Exists(Application.persistentDataPath + _path) || !File.Exists(Application.persistentDataPath + _defaultPath))
                StartCoroutine(ChangeLocationOfFIle());
        }

        GetDataFromJsonFile(_path);


        

    }


    private void Start()
    {
        //if (PlayerPrefs.GetInt("FirstStartGems") == 0)
        //{
        //    HardCurrencyManager.hardCurrencyValue += 50;
        //    PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
        //    PlayerPrefs.SetInt("FirstStartGems", 1);
        //}
    }

    private IEnumerator ChangeLocationOfFIle()
    {
        WWW path = new WWW("");
        byte[] _byteArray;

        switch (Application.platform)
        {
            case RuntimePlatform.Android:
                {
                    path = new WWW("jar:file://" + Application.dataPath + "!/assets/Resources/PlayerData/PlayerData.json");

                    while (!path.isDone)
                    {
                    }
                    if (!string.IsNullOrEmpty(path.error))
                    {
                        print(path.error);
                    }
                    else
                    {
                        _byteArray = path.bytes;
                        print("_bytearray");

                        var filepath = string.Format("{0}/{1}/{2}/{3}.{4}", Application.persistentDataPath, "Resources",
                            "PlayerData", "PlayerData", "json");
                        Directory.CreateDirectory(Path.GetDirectoryName(filepath));
                        File.WriteAllBytes(filepath, _byteArray);
                    }


                    path = new WWW("jar:file://" + Application.dataPath + "!/assets/Resources/PlayerData/DefaultData.json");

                    while (!path.isDone)
                    {
                    }
                    if (!string.IsNullOrEmpty(path.error))
                    {
                        print(path.error);
                    }
                    else
                    {
                        _byteArray = path.bytes;
                        print("_bytearray");

                        var filepath = string.Format("{0}/{1}/{2}/{3}.{4}", Application.persistentDataPath, "Resources",
                            "PlayerData", "DefaultData", "json");
                        Directory.CreateDirectory(Path.GetDirectoryName(filepath));
                        File.WriteAllBytes(filepath, _byteArray);
                    }
                    break;
                }
        }

        yield return null;
    }

    public void SaveDataToJsonFile(long bitcoins, string goName, int curLevel)
    {
        PlayerProgress.PlayerBitCoins = bitcoins;
        PlayerProgress.PlayerUnitsProgressData.Find(x => x.UnitNameInHirearchy.ToLower() == goName.ToLower()).UnitLevel = curLevel;

        CreateAndWriteFile(JsonUtility.ToJson(PlayerProgress));
        GetDataFromJsonFile(_path);
    }

    public void GetDataFromJsonFile(string path)
    {
        var contentFromFile = "";

        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            contentFromFile = File.ReadAllText(Application.dataPath + path);
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            contentFromFile = File.ReadAllText(Application.persistentDataPath + path);
        }
        PlayerProgress = JsonUtility.FromJson<Player>(contentFromFile);
    }

    public void ResetAllData()
    {
        #region Read all default data
        var contentFromFile = "";

        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            contentFromFile = File.ReadAllText(Application.dataPath + _defaultPath);
        }
        else
        {
            contentFromFile = File.ReadAllText(Application.persistentDataPath + _defaultPath);
        }
        PlayerProgress = JsonUtility.FromJson<Player>(contentFromFile);
        #endregion

        #region Write all new data in main file
        CreateAndWriteFile(JsonUtility.ToJson(PlayerProgress));
        GetDataFromJsonFile(_path);
        #endregion

        #region UpdateUI
        UpgradeController.Instance.SetSpriteByLevel();
        UI_Fill_Controller.Instance.UpdateUI();
        IEnumerator checkForUpdate = UpgradeController.Instance.CheckForAvailableUpgrade();
        StartCoroutine(checkForUpdate);
        #endregion
    }

    private void CreateAndWriteFile(string json)
    {
        string path = "";
        string folderPath = "/Resources/PlayerData/";

        if (Application.platform == RuntimePlatform.Android)
        {
            path = Application.persistentDataPath + folderPath;
        }
        else
        {
            path = Application.dataPath + folderPath;
        }

        byte[] bytesarray = Encoding.UTF8.GetBytes(json);

        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        print(path);
        //Create file with name and convert to json
        FileStream stream =
            new FileStream(path + "PlayerData.json",
                FileMode.Create);
        stream.Write(bytesarray, 0, bytesarray.Length);
        stream.Close();
    }

    [System.Serializable]
    public class Player
    {
        public long PlayerBitCoins;

        public List<Units> PlayerUnitsProgressData;

        public Player(long _playerBitCoins, List<Units> _playerUnits)
        {
            PlayerBitCoins = _playerBitCoins;
            PlayerUnitsProgressData = _playerUnits;
        }

    }

    [System.Serializable]
    public class Units
    {
        public string UnitNameInHirearchy;
        public int UnitLevel;

        public Units(string _unitNameInHirearchy, int _unitLevel)
        {
            UnitNameInHirearchy = _unitNameInHirearchy;
            UnitLevel = _unitLevel;
        }
    }

}
