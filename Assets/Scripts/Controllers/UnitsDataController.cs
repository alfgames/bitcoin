﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class UnitsDataController : MonoBehaviour
{
    public static UnitsDataController Instance;
    public JsonData DataObject;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        GetDataFromJson();
    }

    public void GetDataFromJson()
    {
        var contentFromFile = "";
        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            contentFromFile = File.ReadAllText(Application.dataPath + "/Resources/JsonData/UnitsData.json");
        }
        else
        {
            TextAsset textAsset = (TextAsset)Resources.Load(("JsonData/UnitsData"), typeof(TextAsset));
            contentFromFile = textAsset.text;
            
        }
        print("ADAD" + contentFromFile);
        DataObject = JsonUtility.FromJson<JsonData>(contentFromFile);
    }

    [Serializable]
    public class JsonData
    {
        public List<UnitsData> UnitsDataArray = new List<UnitsData>();
    }

    [Serializable]
    public class UnitsData
    {
        public string LocationName;
        public List<Unit> Units;
    }

    [Serializable]
    public class Unit
    {
        public string NameInHierarchy;
        public List<UnitLanguagesName> AllLanguages;
        public List<UnitLevel> AllLevels;
    }

    [Serializable]
    public class UnitLevel
    {
        public int LevelNumber;
        public int LevelBonus;
        public int LevelPrice;
    }

    [Serializable]
    public class UnitLanguagesName
    {
        public string Language;
        public string Name;
    }
}
