﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class MailsController : MonoBehaviour
{
    public enum MailType
    {
        None,
        BonusX30,
        BonusX50,
        Multiply10,
        Multiply2,
        Multiply5,
        TapX10,
        TapX20,
        Minus13,
        Minus23,
        Minus14
    }
    public MailType CurrentMailType;

    [Header("Script Components")]
    public Balance BalanceComponent;

    [Space(10f)]
    [Header("UI")]
    public Button[] ButtonsToDeactivate;
    public Button LetterButton;

    public Button AcceptGoodMailButton;
    public Button AcceptBadMailButton;
    public Button CancelBadMailButton;

    public GameObject GoodMailPanel;
    public GameObject BadMailPanel;

    public Text GoodMailDescription;
    public Text BadMailDescription;


    public Animator LetterImage;

    [Space(10f)]
    public bool IsTimerEnd;

    [SerializeField]
    private string[] ActionDescription;

    public void Start()
    {
        Mail();
    }

    public void CallMail()
    {
        Invoke("Mail", 30f);
    }

    private void Mail()
    {
        if (GameObject.FindGameObjectWithTag("Sound") != null)
        {
            AudioManager.Instance.PlayAudio(AudioManager.Instance.mailCreate);
        }

        LetterImage.gameObject.SetActive(true);
        LetterImage.SetTrigger("MailAppear");

        int random = Random.Range(1, 11);
        CurrentMailType = ChooseType(random);
        //CurrentMailType = MailType.Minus14;

        LetterButton.onClick.RemoveAllListeners();
        LetterButton.onClick.AddListener(() => OpenPanelByType(CurrentMailType, random));

    }

    private void OpenPanelByType(MailType type, int typeNumber)
    {

        if (GameObject.FindGameObjectWithTag("Sound") != null)
        {
            AudioManager.Instance.PlayAudio(AudioManager.Instance.mailOpen);
        }

        StatisticsController.Instance.IncreaseMailOpened();
        switch (type)
        {
            case MailType.BonusX30:
            case MailType.BonusX50:
            case MailType.Multiply10:
            case MailType.Multiply2:
            case MailType.Multiply5:
            case MailType.TapX10:
            case MailType.TapX20:
                GoodMailPanel.SetActive(true);
                GoodMailDescription.text = ActionDescription[typeNumber - 1];
                AcceptGoodMailButton.onClick.AddListener(() =>
                {
                    SetActionByType(type);
                    GoodMailPanel.SetActive(false);
                    AcceptGoodMailButton.onClick.RemoveAllListeners();
                    LetterImage.gameObject.SetActive(false);
                    CallMail();
                });
                break;
            case MailType.Minus13:
            case MailType.Minus23:
            case MailType.Minus14:
                BadMailPanel.SetActive(true);
                BadMailDescription.text = ActionDescription[typeNumber - 1];
                AcceptBadMailButton.onClick.AddListener(() =>
                {
                    SetActionByType(type);
                    BadMailPanel.SetActive(false);
                    AcceptBadMailButton.onClick.RemoveAllListeners();
                    LetterImage.gameObject.SetActive(false);
                    CallMail();
                });

                CancelBadMailButton.onClick.AddListener(() =>
                {
                    BadMailPanel.SetActive(false);
                    CancelBadMailButton.onClick.RemoveAllListeners();
                    LetterImage.gameObject.SetActive(false);
                    CallMail();
                });

                break;
        }
    }

    private void SetActionByType(MailType curType)
    {
        switch (curType)
        {
            case MailType.BonusX30:
                StartCoroutine(BonusXAddValue(30, true, true, 20f));
                break;
            case MailType.BonusX50:
                StartCoroutine(BonusXAddValue(50, true, true, 10f));
                break;
            case MailType.Multiply10:
                StartCoroutine(BonusXAddValue(10, true, true, 1f));
                break;
            case MailType.Multiply2:
                StartCoroutine(BonusXAddValue(20, true, true, 1f));
                break;
            case MailType.Multiply5:
                StartCoroutine(BonusXAddValue(50, true, true, 1f));
                break;
            case MailType.TapX10:
                StartCoroutine(BonusXTap(10, true, true, 30));
                break;
            case MailType.TapX20:
                StartCoroutine(BonusXTap(20, true, true, 20));
                break;
            case MailType.Minus13:
                MinusBalance(1f / 3);
                break;
            case MailType.Minus23:
                MinusBalance(2f / 3);
                break;
            case MailType.Minus14:
                MinusBalance(1f / 4);
                break;
            default:
                throw new ArgumentOutOfRangeException("curType", curType, null);
        }
    }

    #region MailActions

    public IEnumerator BonusXAddValue(long value, bool temporary, bool isMultiply, float time = 0f)
    {

        if (GameObject.FindGameObjectWithTag("Sound") != null)
        {
            AudioManager.Instance.PlayAudio(AudioManager.Instance.okSound);
        }

        BalanceComponent.AdditionalValue = (isMultiply) ? BalanceComponent.AdditionalValue * value : BalanceComponent.AdditionalValue + value;
        BalanceComponent.WriteBalanceValues();
        if (temporary)
        {
            StartCoroutine(Timer(time));
            yield return new WaitUntil(() => IsTimerEnd);

            BalanceComponent.AdditionalValue = (isMultiply) ? BalanceComponent.AdditionalValue / value : BalanceComponent.AdditionalValue - value;
            BalanceComponent.WriteBalanceValues();
        }
    }

    public IEnumerator BonusXTap(int value, bool temporary, bool isMultiply, float time = 0f)
    {
        if (GameObject.FindGameObjectWithTag("Sound") != null)
        {
            AudioManager.Instance.PlayAudio(AudioManager.Instance.okSound);
        }

        BalanceComponent.TaplValue = (isMultiply) ? BalanceComponent.TaplValue * value : BalanceComponent.TaplValue + value;
        BalanceComponent.WriteBalanceValues();
        if (temporary)
        {
            StartCoroutine(Timer(time));
            yield return new WaitUntil(() => IsTimerEnd);

            BalanceComponent.TaplValue = (isMultiply) ? BalanceComponent.TaplValue / value : BalanceComponent.TaplValue - value;
            BalanceComponent.WriteBalanceValues();
        }
    }

    private void MinusBalance(float minusCoef)
    {


        if (GameObject.FindGameObjectWithTag("Sound") != null)
        {
            AudioManager.Instance.PlayAudio(AudioManager.Instance.okSound);
        }

        BalanceComponent.BalanceValue -= (Convert.ToInt64(BalanceComponent.BalanceValue * minusCoef));
        BalanceComponent.WriteBalanceValues();
    }

    public void PlusBalance(long value, int multiply)
    {


        if (GameObject.FindGameObjectWithTag("Sound") != null)
        {
            AudioManager.Instance.PlayAudio(AudioManager.Instance.okSound);
        }

        BalanceComponent.BalanceValue += (value * multiply);
        BalanceComponent.WriteBalanceValues();
    }

    public IEnumerator IncreaseInvestments(bool temporary, float time = 0f)
    {

        if (GameObject.FindGameObjectWithTag("Sound") != null)
        {
            AudioManager.Instance.PlayAudio(AudioManager.Instance.okSound);
        }

        PlayerPrefs.SetInt("IncreaseInvestments", 10);
        BalanceComponent.NewAdditionalIncome();
        if (temporary)
        {
            StartCoroutine(Timer(time));
            yield return new WaitUntil(() => IsTimerEnd);

            PlayerPrefs.DeleteKey("IncreaseInvestments");
            BalanceComponent.NewAdditionalIncome();

        }
    }

    #endregion


    private MailType ChooseType(int number)
    {
        MailType choosenType = MailType.None;
        switch (number)
        {
            case 1:
                choosenType = MailType.BonusX30;
                break;
            case 2:
                choosenType = MailType.BonusX50;
                break;
            case 3:
                choosenType = MailType.Multiply10;
                break;
            case 4:
                choosenType = MailType.Multiply2;
                break;
            case 5:
                choosenType = MailType.Multiply5;
                break;
            case 6:
                choosenType = MailType.TapX10;
                break;
            case 7:
                choosenType = MailType.TapX20;
                break;
            case 8:
                choosenType = MailType.Minus13;
                break;
            case 9:
                choosenType = MailType.Minus23;
                break;
            case 10:
                choosenType = MailType.Minus14;
                break;
        }
        return choosenType;
    }

    IEnumerator Timer(float time)
    {
        IsTimerEnd = false;
        ActivateDeactivateButtons();
        while (time > 0.0f)
        {
            time -= Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
        IsTimerEnd = true;
        ActivateDeactivateButtons();
        yield return null;
    }

    private void ActivateDeactivateButtons()
    {
        foreach (var item in ButtonsToDeactivate)
        {
            item.interactable = !item.interactable;
        }
    }

}
