﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UI_Fill_Controller : MonoBehaviour
{
    public static UI_Fill_Controller Instance;
    [Header("Player UI")] public Text BitcoinsText;

    [Header("Upgrade Panel")]

    public Text ObjectName;
    public Text PriceText;
    public Text BonusText;

    public Image NextUnit;

    public Interface InterfaceComponent;

    public UnitsDataController UDCComponent;
    public UnitsDataController.UnitLevel DataByLevel;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        UpdateUI();
    }

    public void FillUI(GameObject goName)
    {
        UnitsDataController.Unit CurrentObject = null;
        foreach (var item in UDCComponent.DataObject.UnitsDataArray)
            if (item.LocationName.ToLower() == SceneManager.GetActiveScene().name.ToLower())
            {
                foreach (var i in item.Units)
                    if (i.NameInHierarchy.ToLower() == goName.name.ToLower())
                        CurrentObject = i;
                break;
            }
        //var CurrentObject = UDCComponent.DataObject.UnitsDataArray.Find(x => x.LocationName.ToLower() == SceneManager.GetActiveScene().name.ToLower()).Units.Find(a => a.NameInHierarchy.ToLower() == goName.name.ToLower());
        if (CurrentObject != null)
        {
            foreach (var lang in CurrentObject.AllLanguages)
                if (lang.Language == Application.systemLanguage.ToString().ToLower())
                {
                    ObjectName.text = lang.Name;
                    break;
                }
            int lv = 0;
            foreach (var l in PlayerDataController.Instance.PlayerProgress.PlayerUnitsProgressData)
                if (l.UnitNameInHirearchy.ToLower() == goName.name.ToLower())
                {
                    lv = l.UnitLevel;
                    break;
                }
            foreach (var level in CurrentObject.AllLevels)
            {
                if (level.LevelNumber == lv)
                {
                    DataByLevel = level;
                    break;
                }
            }
            //if (CurrentObject.AllLanguages != null)
            //    ObjectName.text = CurrentObject.AllLanguages.Find(y => y.Language.ToLower() == Application.systemLanguage.ToString().ToLower()).Name;
            //if (CurrentObject.AllLevels != null)
            //   DataByLevel = CurrentObject.AllLevels.Find(z => z.LevelNumber == PlayerDataController.Instance.PlayerProgress.PlayerUnitsProgressData.Find(b => b.UnitNameInHirearchy.ToLower() == goName.name.ToLower()).UnitLevel);
        }
        PriceText.text = InterfaceComponent.FormattingValue(DataByLevel.LevelPrice);
        BonusText.text = InterfaceComponent.FormattingValue(DataByLevel.LevelBonus); 
        NextUnit.sprite = UpgradeController.Instance.FindNextSprite(goName.name, DataByLevel.LevelNumber);
        UpgradeController.Instance.UpgradePanel.SetActive(true);
        UpgradeController.Instance.UpgradeButton.onClick.RemoveAllListeners();
        UpgradeController.Instance.UpgradeButton.onClick.AddListener(() => UpgradeController.Instance.UpgradeUnit(DataByLevel.LevelPrice,goName.name, DataByLevel.LevelNumber+1));
    }

    public void UpdateUI()
    {
        BitcoinsText.text = PlayerDataController.Instance.PlayerProgress.PlayerBitCoins.ToString();
    }
}
