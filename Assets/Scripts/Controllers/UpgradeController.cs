﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UpgradeController : MonoBehaviour
{

    public static UpgradeController Instance;

    public List<Image> SceneUnits;
    public List<Sprite> UpgradeUnits;
    public List<Sprite> coincidence = new List<Sprite>();

    public GameObject UpgradePanel;
    public GameObject NotEnoughMoneyPanel;
    public GameObject BonusesPanel;

    public Button UpgradeButton;

    public Balance BalanceComponent;

    private void Awake()
    {
        Instance = this;
        SetSpriteByLevel();
        StartCoroutine(CheckForAvailableUpgrade());
    }

    public void UpgradeUnit(int unitPrice, string goName, int level)
    {
        if (IsCanUpgradeUnit(BalanceComponent.BalanceValue, unitPrice))
        {

            if (GameObject.FindGameObjectWithTag("Sound") != null)
            {
                AudioManager.Instance.PlayAudio(AudioManager.Instance.upgradeRoomsItem);
            }


            BalanceComponent.BalanceValue -= unitPrice;
            StatisticsController.Instance.IncreaseSpendMoney(unitPrice);

            BalanceComponent.AdditionalValue += UI_Fill_Controller.Instance.DataByLevel.LevelBonus;
            print(BalanceComponent.AdditionalValue + " " + UI_Fill_Controller.Instance.DataByLevel.LevelBonus);
            BalanceComponent.WriteBalanceValues();

            PlayerDataController.Instance.SaveDataToJsonFile(BalanceComponent.BalanceValue, goName, level);

            UpgradePanel.SetActive(false);
            SetSpriteByLevel();
            UI_Fill_Controller.Instance.UpdateUI();
            StartCoroutine(CheckForAvailableUpgrade());
        }
        else
        {
            NotEnoughMoneyPanel.SetActive(true);
            Button notEnoughBtn = NotEnoughMoneyPanel.GetComponentInChildren<Button>();
            notEnoughBtn.onClick.RemoveAllListeners();
            notEnoughBtn.onClick.AddListener(() =>
            {
                NotEnoughMoneyPanel.SetActive(false);
                BonusesPanel.SetActive(true);
            });
        }
    }

    private bool IsCanUpgradeUnit(long playerBC, long unitPrice)
    {
        if (playerBC > unitPrice)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public Sprite FindNextSprite(string nameOfObject, int nextLevel)
    {
        Sprite nextSprite;

        coincidence.Clear();

        foreach (var s in UpgradeUnits)
        {
            var curSprite = ContainsWord(s, nameOfObject);
            if (curSprite != null)
            {
                coincidence.Add(curSprite);
            }
        }

        nextSprite = coincidence.Find(x => x.name.ToLower().Contains(nameOfObject.ToLower()) && x.name.Contains(nextLevel.ToString()));

        return nextSprite;
    }

    public Sprite ContainsWord(Sprite curreSprite, string word)
    {
        string[] ar = curreSprite.name.Split('_');

        foreach (string str in ar)
        {
            if (str.ToLower() == word.ToLower())
                return curreSprite;
        }
        return null;
    }



    public void SetSpriteByLevel()
    {
        foreach (var item in SceneUnits)
        {
            var path = "Images/" + SceneManager.GetActiveScene().name + "/" + item.name + "/";
            var levelOfObject =
                PlayerDataController.Instance.PlayerProgress.PlayerUnitsProgressData.Find(
                    x => x.UnitNameInHirearchy == item.name).UnitLevel;
            item.sprite = Resources.Load(path + levelOfObject, typeof (Sprite)) as Sprite;
        }
    }

    public IEnumerator CheckForAvailableUpgrade()
    {
        yield return new WaitForSeconds(.1f);
        foreach (var item in SceneUnits)
        {
            var levelOfObject =
                PlayerDataController.Instance.PlayerProgress.PlayerUnitsProgressData.Find(
                    x => x.UnitNameInHirearchy.ToLower() == item.name.ToLower()).UnitLevel;
            var CurrentObject =
             UnitsDataController.Instance.DataObject.UnitsDataArray.Find(x => x.LocationName.ToLower() == SceneManager.GetActiveScene().name.ToLower()).Units.Find(a => a.NameInHierarchy.ToLower() == item.name.ToLower());


            if (levelOfObject <= CurrentObject.AllLevels.Count)
            {
                item.GetComponent<Button>().enabled = true;
            }
            else
            {
                item.GetComponent<Button>().enabled = false;
            }
        }
    }
}
