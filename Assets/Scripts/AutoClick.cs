﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoClick : MonoBehaviour {
    public static AutoClick Instance;
    public Click click;
    public Clicker clicker;
    public Text timer;
    public GameObject[] activeOBJ;
     //void Update()
    //{
    //    fundsDisplay.text = "" + CurrencyConverter.Instance.GetCurrencyIntoString(funds, false, false);
    //    fpc.text = CurrencyConverter.Instance.GetCurrencyIntoString(fundsPerClick, false, true) + " /Click";
    //}
    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        foreach (var v in activeOBJ)
            v.SetActive(false);
       //ClickAuto(10, 1);
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    Coroutine at;
    float totalTime;
    public void ClickAuto(float inTime, float currperclick, int countpersecond = 10)
    {
        foreach (var v in activeOBJ)
            v.SetActive(true);
        if (at != null) StopCoroutine(at);
        totalTime += inTime;
        at = StartCoroutine(Auto(totalTime, currperclick, countpersecond));
    }

    IEnumerator Auto(float inTime, float currperclick, int countpersecond = 2)
    {
        while(totalTime > 0)
        {
            totalTime -= 1f / currperclick;
            timer.text = (int)totalTime / 60 +":"+ totalTime % 60;
            click.Clicked(false);
            clicker.ClickClick();
            yield return new WaitForSeconds(1f / currperclick);
        }
        foreach (var v in activeOBJ)
            v.SetActive(false);
        yield break;
    }
}
