﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.IO;

public class Balance : MonoBehaviour {

    public Interface _interface;
    public Database dataBase;
    public OfflineFarming offlineFarm;
    int standardIncome = 0;

    private long additionalIncome;
    private long balanceValue;
    private int _tapValue;

    DateTime realTime;
    DateTime lastTime;
    int time = 1;

    private readonly string BalanceFilePath = "/Informations/Balance.txt";

    void Start()
    {
        ChangeFileLocation();
        CheckLastBalanceValue();
        CheckNotIssuedCoins();

        Debug.Log(balanceValue);
        balanceValue += 500000000000000000;
        //balanceValue += Int64.Parse(PlayerPrefs.GetString("BonusGameValue"));
    }

    private void ChangeFileLocation()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (!File.Exists(Application.persistentDataPath + BalanceFilePath))
            {
                print("not exist");
                StartCoroutine(ChangeLocationOfFIle());
            }
            else
            {
                print(" exist");

            }
        }
    }

    public TextAsset textBalance;
    private IEnumerator ChangeLocationOfFIle()
    {
        var filepath = string.Format("{0}/{1}/{2}.{3}", Application.persistentDataPath, "Informations", "Balance", "txt");
        Directory.CreateDirectory(Path.GetDirectoryName(filepath));
        File.WriteAllBytes(filepath, textBalance.bytes);
        yield return null;
    }

    void Update()
    {
        Timer();
        CreateNewBalanceText();
    }





    private void ReadBalanceValues()
    {
        StreamReader fs;
        if (Application.platform == RuntimePlatform.Android)
        {
            fs = new StreamReader(Application.persistentDataPath + BalanceFilePath);
        }
        else
        {
            fs = new StreamReader(Application.dataPath + @"\Informations\Balance.txt");
        }
        string text = fs.ReadToEnd();
        string[] stringSeparators = new string[] { "\r\n", "\n" };
        string[] textArray = text.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

        balanceValue = Convert.ToInt64 (textArray[0]);
        additionalIncome = Convert.ToInt64(textArray[1]);
        _tapValue = Convert.ToInt32(textArray[2]);

        fs.Close();
    }

    public void WriteBalanceValues()
    {
        StreamReader fs;
        if (Application.platform == RuntimePlatform.Android)
        {
            fs = new StreamReader(Application.persistentDataPath + BalanceFilePath);
        }
        else
        {
            fs = new StreamReader(Application.dataPath + @"\Informations\Balance.txt");
        }
        string text = fs.ReadToEnd();
        string[] stringSeparators = new string[] { "\r\n", "\n" };
        string[] textArray = text.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
        fs.Close();

        textArray[0] = balanceValue.ToString();
        textArray[1] = additionalIncome.ToString();
        textArray[2] = _tapValue.ToString();
        textArray[3] = _investmentValue.ToString();


        StreamWriter outputFile;
        if (Application.platform == RuntimePlatform.Android)
        {
            outputFile = new StreamWriter(Application.persistentDataPath + BalanceFilePath);

        }
        else
        {
            outputFile = new StreamWriter(Application.dataPath + @"\Informations\Balance.txt");
        }
        foreach (string line in textArray)
        {
            outputFile.WriteLine(line);
        }
        outputFile.Close();
    }

    private void AddNewCoins( long offlineBonus = 0)
    {
        var addValues = additionalIncome + offlineBonus;
        balanceValue += addValues;
        StatisticsController.Instance.IncreaseEarnedMoney(addValues);
        WriteBalanceValues();
    }

    private void CheckLastBalanceValue()
    {
        ReadBalanceValues();
    }

    private void CheckNotIssuedCoins()
    {
        if (PlayerPrefs.GetString("lastTime") == null)
        {
            lastTime = DateTime.Now;
        }
        else
        {
            lastTime = Convert.ToDateTime(PlayerPrefs.GetString("lastTime"));

            double timeSpan = (DateTime.Now - lastTime).TotalSeconds;
            long notIssuedCoins = offlineFarm.OfflineBonus(timeSpan);

            AddNewCoins(notIssuedCoins);
        }
    }

    private void Timer()
    {
        double timeSpan = (DateTime.Now - lastTime).TotalSeconds;

        if (timeSpan > time)
        {
            lastTime = DateTime.Now;
            PlayerPrefs.SetString("lastTime", Convert.ToString(lastTime));
            AddNewCoins();
        }
    }

    private void CreateNewBalanceText()
    { 
        _interface.ChangeBalanceValueText(balanceValue);
    }


    public long BalanceValue
    {
        get { return balanceValue; }
        set { balanceValue = value; }
    }

    public long AdditionalValue
    {
        get { return additionalIncome; }
        set { additionalIncome = value; }
    }

    public int TaplValue
    {
        get { return _tapValue; }
        set { _tapValue = value; }
    }

    private long _investmentValue;
    public long InvestmentValue
     {
        get { return  _investmentValue; }
        set
        {
            _investmentValue = value;
        }
    }

    public void NewAdditionalIncome()
    {
        AdditionalValue -= InvestmentValue;

        _investmentValue = standardIncome;

        var multiply = 1;
        if(PlayerPrefs.HasKey("InvestmentMultiply"))
        {
            multiply *= 2;
        }

        if (PlayerPrefs.HasKey("IncreaseInvestments"))
        {
            multiply *= 10;
        }


        for (int i = 0; i < dataBase.myInvestments.Count; i++)
        {
            InvestmentValue += dataBase.myInvestments[i].bonusIncome * multiply;

        }

        AdditionalValue += InvestmentValue;
    }
}
