using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadingScreenManager : MonoBehaviour {

    //public Image loadingBar;
    public Text progressText;
    private string levelName;

    public GameObject warningWin;

    private void Awake()
    {
        levelName = PlayerPrefs.GetString("LoadLevel");

        if (PlayerPrefs.GetInt("Tutorial") == 0)
        {
            warningWin.SetActive(true);
        }
    }


    IEnumerator Start()
    {
        AsyncOperation ao = SceneManager.LoadSceneAsync(levelName);
        ao.allowSceneActivation = false;

        while (!ao.isDone)
        {
            float progress = Mathf.Clamp01(ao.progress / 0.9f);
            progressText.text = "Loading: " + (progress * 100).ToString("F0") + " %";
            //loadingBar.fillAmount = ao.progress;
            if (ao.progress == 0.9f)
            {
                ao.allowSceneActivation = true;
            }
            yield return null;
        }
    }

}