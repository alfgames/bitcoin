﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CataclismManager : MonoBehaviour {

    public GameObject[] cataclisms;
    public float cataclismSpawnTime;

    private float timer;

    private void Start()
    {
        timer = 0;
        PlayerPrefs.SetInt("TimerState", 0);
    }

    void Update () {
        

        if (PlayerPrefs.GetInt("TimerState") == 0)
        {
            timer += Time.deltaTime;
            if (timer >= cataclismSpawnTime)
            {
                CreateCataclism();
                timer = 0;
                PlayerPrefs.SetInt("TimerState", 1);
            }
        }
    }

    public void CreateCataclism()
    {
        int randCataclysm = Random.Range(0, cataclisms.Length);
        GameObject c = Instantiate(cataclisms[randCataclysm]);
    }
}
