﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.IO;

public class OfflineFarming : MonoBehaviour {

    public Database database;
    public Interface _interface;
    public Balance balance;

    private List<Farming> offlineFarmingInform;

    public int offlineFarmingLevel;
    int maxOfflineFarmingLvl = 5;

    private readonly string OfflineFarmingFilePath = "/Informations/Offline Farming.txt";

    void Awake()
    {
        ChangeFileLocation();
        ReadOfflineFarmingValues();
        GetOfflineFarmingLevel();
        if (!PlayerPrefs.HasKey("OfflineFarmingLevel"))
        {
            SetOfflineFarmingLevel(1);
        }
    }

    private void ChangeFileLocation()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (!File.Exists(Application.persistentDataPath + OfflineFarmingFilePath))
            {
                StartCoroutine(ChangeLocationOfFIle());
            }
        }
    }

    public TextAsset farming;
    private IEnumerator ChangeLocationOfFIle()
    {
        var filepath = string.Format("{0}/{1}/{2}.{3}", Application.persistentDataPath, "Informations",
                            "Offline Farming", "txt");
        Directory.CreateDirectory(Path.GetDirectoryName(filepath));
        File.WriteAllBytes(filepath, farming.bytes);
        yield return null;
    }

    private void ReadOfflineFarmingValues()
    {
        StreamReader fs;
        if (Application.platform == RuntimePlatform.Android)
        {
            fs = new StreamReader(Application.persistentDataPath + OfflineFarmingFilePath);

        }
        else
        {
            fs = new StreamReader(Application.dataPath + "/Informations/Offline Farming.txt");
        }

        string text = fs.ReadToEnd();
        string[] stringSeparators = new string[] { "\r\n", "\n" };
        string[] textArray = text.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
        int semicolonIndex;

        offlineFarmingInform = new List<Farming>();

        for (int i = 0; i < maxOfflineFarmingLvl + 1; i++)
        {
            semicolonIndex = textArray[i].IndexOf(";");
            string tempString = textArray[i];
            int percent = Convert.ToInt32(tempString.Remove(semicolonIndex));

            textArray[i] = textArray[i].Substring(semicolonIndex + 1);
            tempString = textArray[i];
            int time = Convert.ToInt32(tempString.Substring(0, tempString.Length - 1));

            offlineFarmingInform.Add(new Farming(percent, time));
        }

        fs.Close();
    }

    public void SetOfflineFarmingLevel(int value)
    {
        offlineFarmingLevel = value;
        PlayerPrefs.SetInt("OfflineFarmingLevel", value);
        Debug.Log("set value = " + value);
    }

    public void GetOfflineFarmingLevel()
    {
         offlineFarmingLevel = PlayerPrefs.GetInt("OfflineFarmingLevel");
    }

    public long OfflineBonus(double time)
    {
        long bonus = 0;
        float percent = offlineFarmingInform[offlineFarmingLevel].percent ;

        if (Math.Round( time ) >= offlineFarmingInform[offlineFarmingLevel].time * 60)
        {
            bonus = Convert.ToInt64((offlineFarmingInform[offlineFarmingLevel].time * 60) * percent / 100 * balance.AdditionalValue);
        }
        else
        {
            bonus = Convert.ToInt64(Convert.ToInt64(time) * percent / 100 * balance.AdditionalValue);
        }

        return bonus;
    }

}
