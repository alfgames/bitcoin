﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdsManager : MonoBehaviour
{
    public MailsController MailsControllerComponent;
    System.Action callBack;
    public Button x5Button;
    bool autoShow = false;
    public void x5Diamond()
    {

        //ADSController.Instance.RequestRewardBasedVideo(true, (st, sr) => {
        //    HardCurrencyManager.hardCurrencyValue += 5;
        //    PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
        //    AutoClick.Instance.ClickAuto(60, 3);
        //});

        //autoShow = true;   
        //callBack = () =>
        //{
        //    HardCurrencyManager.hardCurrencyValue += 5;
        //    PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
        //    AutoClick.Instance.ClickAuto(60, 3);
        //};
        //PreloadOrShowRewardedInterstitial();
    }

    public void showRewardedVideo()
    {
        PreloadOrShowRewardedInterstitial();
        //callBack = () => {

        //};
    }

    // Put AppLovin SDK Key here or in your AndroidManifest.xml / Info.plist
    private const string SDK_KEY = "d6bXGyNUki3LU4NRya_m-3xp_E43TJl6QKehH1A_kDLCIieEId_Ghx4g98kcRTkspMX4Ni46xPID7ySPaRgGNm";

    // Rewarded Video Button Texts
    private const string REWARDED_VIDEO_BUTTON_TITLE_PRELOAD = "Preload Rewarded Video";
    private const string REWARDED_VIDEO_BUTTON_TITLE_LOADING = "Show Rewarded Video";
    private const string REWARDED_VIDEO_BUTTON_TITLE_SHOW = "Show Rewarded Video";

    // Controlled State
    private bool IsPreloadingRewardedVideo = false;

    void Start()
    {
        // Set SDK key and initialize SDK
        AppLovin.SetSdkKey(SDK_KEY);
        AppLovin.InitializeSdk();
        //AppLovin.SetTestAdsEnabled("true");
        AppLovin.SetUnityAdListener("AdsManager");
        //AppLovin.SetRewardedVideoUsername("demo_user");
        //PreloadOrShowRewardedInterstitial();
        Invoke("ShowInterstitial", Random.Range(120f, 180f));
    }

    public void ShowInterstitial()
    {
        print("Showing interstitial ad");
        //AppLovin.ShowInterstitial();
       // ADSController.Instance.RequestInterstitial(true);
        FBTracking.Instance.LogAd_shown_interstitialEvent();
        Invoke("ShowInterstitial", Random.Range(120f, 180f));
    }

    public void PreloadOrShowRewardedInterstitial()
    {
        if (AppLovin.IsIncentInterstitialReady())
        {

            print("Showing rewarded ad...");

            IsPreloadingRewardedVideo = false;
            autoShow = false;
            AppLovin.ShowRewardedInterstitial();
            FBTracking.Instance.LogRewarded_shownEvent();
            x5Button.interactable = false;
        }
        else
        {

            print("Preloading rewarded ad...");

            IsPreloadingRewardedVideo = true;
            AppLovin.LoadRewardedInterstitial();
            x5Button.interactable = false;
        }
    }

    public void ShowBanner()
    {
        print("Showing banner ad");
        AppLovin.ShowAd(AppLovin.AD_POSITION_CENTER, AppLovin.AD_POSITION_BOTTOM);
    }

    private void onAppLovinEventReceived(string ev)
    {
        print(ev);
        if (ev.Contains("REWARDAPPROVED"))
        {
            if (callBack != null)
            {
                callBack();
                callBack = null;
            }
        }

        if (ev.Contains("VIDEOSTOPPED") && !AppLovin.IsIncentInterstitialReady())
            PreloadOrShowRewardedInterstitial();
        // Check if this is a Rewarded Video preloading event
        else if (IsPreloadingRewardedVideo && (ev.Equals("LOADED") || ev.Equals("LOADFAILED")))
        {
            IsPreloadingRewardedVideo = false;
            if (autoShow && AppLovin.IsIncentInterstitialReady())
                PreloadOrShowRewardedInterstitial();
        }
        x5Button.interactable = AppLovin.IsIncentInterstitialReady();
    }
}
