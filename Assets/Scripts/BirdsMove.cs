﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdsMove : MonoBehaviour {

    public float speed;
    private Vector2 direction;

	void Start () {
        direction = new Vector2(-1f,0);
        Destroy(gameObject, 5f);
	}
	
	void Update () {
        transform.Translate(direction * speed * Time.deltaTime);
	}
}
