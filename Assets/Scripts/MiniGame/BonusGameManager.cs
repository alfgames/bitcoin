﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

public class BonusGameManager : MonoBehaviour {

    public GameObject[] particles;
    public GameObject virusAnim;
    public GameObject loseScreen;
    public GameObject spawmManager;


    public GameObject soundManager;
    public GameObject musicManager;

    public Sprite[] hpSprites; 
    public Image[] hp;

    private int hpCounter;
    private bool isGameOver;
    private int addititionValue;

    void Start () {


        addititionValue = Int32.Parse(PlayerPrefs.GetString("CurrentBalanceValue"));

        Time.timeScale = 1;
        isGameOver = false;
        hpCounter = -1;

        for (int i = 0; i < hp.Length; i++)
        {
            hp[i].sprite = hpSprites[0];
        }

        switch (PlayerPrefs.GetInt("Audio_Sound"))
        {
            case 1:

                soundManager.SetActive(false);
                break;
            case 0:

                soundManager.SetActive(true);
                break;
        }

        switch (PlayerPrefs.GetInt("Music_Sound"))
        {
            case 1:

                musicManager.SetActive(false);
                break;
            case 0:

                musicManager.SetActive(true);
                break;
        }
    }

    void Update()
    {
        if (!isGameOver)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector2 ray = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                RaycastHit2D hit = Physics2D.Raycast(ray, Vector2.zero);

                if (hit)
                {
                    if (hit.collider.tag == "Coin")
                    {
                        OnCoinClick();
                        if (GameObject.FindGameObjectWithTag("Sound") != null)
                        {
                            AudioManager.Instance.PlayAudio(AudioManager.Instance.bonusCoinClick);
                        }
                        GameObject c = Instantiate(particles[0]);
                        c.transform.position = hit.transform.position;
                        Destroy(hit.collider.gameObject);
                        Destroy(c, 1f);
                    }
                    if (hit.collider.tag == "Virus")
                    {
                        hpCounter++;
                        OnVirusClick();
                        if (GameObject.FindGameObjectWithTag("Sound") != null)
                        {
                            AudioManager.Instance.PlayAudio(AudioManager.Instance.bonusVirusClick);
                        }
                        GameObject v = Instantiate(particles[1]);
                        v.transform.position = hit.transform.position;
                        Destroy(hit.collider.gameObject);
                        Destroy(v, 1f);

                    }
                }
            }
        }
        if (hpCounter == 2)
        {
            isGameOver = true;
            loseScreen.SetActive(true);
            if (GameObject.FindGameObjectWithTag("Sound") != null)
            {
                if (GameObject.FindGameObjectWithTag("Music"))
                {
                    GameObject.FindGameObjectWithTag("Music").SetActive(false);
                }


                AudioManager.Instance.PlayAudio(AudioManager.Instance.bonusGameOver);
            }

            PlayerPrefs.SetInt("BonusGameValue", BonusScoreManager.scoreValue);

            Destroy(spawmManager);
            hpCounter = 5;
        }
    }

    void OnCoinClick()
    {
        BonusScoreManager.scoreValue += addititionValue;
    }

    void OnVirusClick()
    {
        virusAnim.GetComponent<Animator>().SetBool("Show", true);
        hp[hpCounter].sprite = hpSprites[1];
        Invoke("ResetVirusAnmi", 0.5f);
    }

    void ResetVirusAnmi()
    {
        virusAnim.GetComponent<Animator>().SetBool("Show", false);
    }
}
