﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

	
	void Start () {
        Time.timeScale = 1;
	}

    public void OpenWinBtn(GameObject win)
    {
        Time.timeScale = 0;
        win.SetActive(true);
    }

    public void CloseWinBtn(GameObject win)
    {
        Time.timeScale = 1;
        win.SetActive(false);
    }

    public void LoadSceneBtn(string sceneName)
    {
        Time.timeScale = 1;
        PlayerPrefs.SetString("LoadLevel", sceneName);
        SceneManager.LoadScene("Loader");
    }

}
