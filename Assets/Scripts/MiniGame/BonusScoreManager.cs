﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusScoreManager : MonoBehaviour {

    public Text scoreText;
    public static int scoreValue;

    private string ending;
    long bigPart;
    long smallPart;

    void Start()
    {
        scoreValue = 0;

    }

    void Update()
    {
        bigPart = scoreValue;
        smallPart = 0;

        if (scoreValue > 1000000000)
        {
            smallPart = bigPart % 1000000000;
            smallPart /= 10000000;
            bigPart /= 1000000000;
            ending = " Gb";
        }
        else if (scoreValue > 1000000)
        {
            smallPart = bigPart % 1000000;
            smallPart /= 10000;
            bigPart /= 1000000;
            ending = " Mb";
        }
        else if (scoreValue > 1000)
        {
            smallPart = bigPart % 1000;
            smallPart /= 100;
            bigPart /= 1000;
            ending = " Kb";
        }
        else
        {
            ending = " B";
        }

        scoreText.text = "x " + bigPart.ToString() + "," + smallPart.ToString() + " " + ending;

    }
}
