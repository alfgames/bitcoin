﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    public GameObject[] coins;

	void Start () {
        InvokeRepeating("Spawn", 3f, 0.25f);
	}
	
    void Spawn()
    {
        int randCoin = Random.Range(0, coins.Length);
        float randPosX = Random.Range(-8f, 8f);
        GameObject g = Instantiate(coins[randCoin]);
        g.transform.position = new Vector2(randPosX, -6f);
        g.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        g.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 750f);
        Destroy(g, 3f);
    }

}
