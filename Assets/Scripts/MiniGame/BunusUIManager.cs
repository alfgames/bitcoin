﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BunusUIManager : MonoBehaviour {

    public GameObject pause;
    public GameObject gameManeger;

    private void Start()
    {
        pause.SetActive(false);
    }

    public void PauseWinBtn()
    {
        gameManeger.SetActive(false);
        pause.SetActive(true);
        Time.timeScale = 0;
    }

    public void ResumeBtn()
    {
        gameManeger.SetActive(true);

        pause.SetActive(false);
        Time.timeScale = 1;
    }

    public void ScenesManager(string sceneName)
    {
       
        Time.timeScale = 1;
        PlayerPrefs.SetInt("BonusGameValue", BonusScoreManager.scoreValue);
        PlayerPrefs.SetString("LoadLevel", sceneName);
        SceneManager.LoadScene("Loader");
    }
}
