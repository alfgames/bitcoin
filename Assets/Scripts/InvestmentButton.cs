﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InvestmentButton : MonoBehaviour {

    public int investmentUpdateNumber;
    private Interface _interface;

    void Start()
    {
        _interface = GameObject.Find("BalanceObjects").transform.Find("Interface").GetComponent<Interface>();
        
    }

    public void OnPointerClick()
    {
        _interface.UpdateInvestment(investmentUpdateNumber);
        

        if (PlayerPrefs.GetInt("Tutorial") == 0)
        {
            GameObject.FindGameObjectWithTag("Tutor6").SetActive(false);
        }
    }
}
