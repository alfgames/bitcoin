﻿using System;
using UnityEngine;
#if UNITY_ANDROID || UNITY_IOS
using GoogleMobileAds.Api;
#elif UNITY_WEBGL
using System.Runtime.InteropServices;
#endif

#if UNITY_WSA
public class ADSController : MonoBehaviour, BackInterfaceController
#else
public class ADSController : MonoBehaviour
#endif
{
    public static ADSController Instance;
    void Awake()
    {
        //DontDestroyOnLoad(this.gameObject);
        Instance = this;
#if UNITY_WSA
        BIC = this;
#endif
    }

#if UNITY_WSA
    public static InterfaceController IC;
    public static BackInterfaceController BIC;


    //interface
    #region Call back from interface
    void BackInterfaceController.InterstitialLoaded()
    {
        print("interstitial loaded");
        interstitialState = AdsState.Loaded;
        if (interstitialAutoShow) ShowInterstitial();
    }

    void BackInterfaceController.InterstitialError()
    {
        print("interstitial error");
        interstitialState = AdsState.Error;
    }

    void BackInterfaceController.InterstitialShowing()
    {
        print("interstitial showing");
        interstitialState = AdsState.None;
    }

    void BackInterfaceController.InterstitialClosed()
    {
        print("interstitial closed");
        interstitialState = AdsState.None;
    }

    void BackInterfaceController.RewardLoaded()
    {
        print("reward loaded");
        rewardState = AdsState.Loaded;
        if (rewardVideoAutoShow) ShowRewardBasedVideo();
    }

    void BackInterfaceController.RewardError()
    {
        print("reward error");
        rewardError = true;
        rewardState = AdsState.Error;

    }

    void BackInterfaceController.RewardShowing()
    {
        print("reward showing");
        rewardError = true;
        rewardState = AdsState.None;
    }

    void BackInterfaceController.RewardClosed()
    {
        print("reward closed");
        rewardError = true;
        rewardState = AdsState.None;
    }

    void FixedUpdate()
    {
        if (rewardCompleted)
        {
            if (this.callBack != null)
                this.callBack(true, "");
            rewardCompleted = false;
        }

        if (rewardError)
        {
            if (this.callBack != null)
                this.callBack(false, "");
            rewardError = false;
        }
    }

    bool rewardCompleted = false, rewardError = false;

    void BackInterfaceController.RewardCompleted()
    {
        print("reward completed");
        rewardCompleted = true;
    }


    #endregion
    bool autoCache = true;

    #region Interstitial
    bool interstitialAutoShow = false;
    AdsState interstitialState = AdsState.None;
    public void RequestInterstitial(bool autoShow)
    {
        if (interstitialState == AdsState.Loaded && autoShow)
        {
            if (IC != null) IC.ShowIntertitial();
            return;
        }

        if (interstitialState == AdsState.Loading) return;
        if (IC != null) IC.RequestIntertitial();
        interstitialAutoShow = autoShow;
        interstitialState = AdsState.Loading;
    }

    public void ShowInterstitial()
    {
        if (interstitialState == AdsState.Loaded)
        {
            if (IC != null) IC.ShowIntertitial();
            interstitialAutoShow = false;
            interstitialState = AdsState.None;
        }
    }


    public void DestroyInterstitial()
    {
        IC.DestroyInterstitial();
    }
    #endregion

    #region Reward Based Video
    bool rewardVideoAutoShow = false;
    AdsState rewardState = AdsState.None;
    Action<bool, string> callBack;
    public void RequestRewardBasedVideo(bool autoShow, Action<bool, string> callBack = null)
    {
        if (rewardState == AdsState.Loaded && autoShow)
        {
            IC.ShowReward();
            return;
        }

        if (rewardState == AdsState.Loading) return;

        IC.RequestReward();
        this.callBack = callBack;
        rewardVideoAutoShow = autoShow;
        rewardState = AdsState.Loading;
    }

    public void ShowRewardBasedVideo()
    {
        if (rewardState == AdsState.Loaded)
        {
            IC.ShowReward();
            rewardState = AdsState.None;
            rewardVideoAutoShow = false;
        }
    }
    #endregion
#endif

#if UNITY_ANDROID || UNITY_IOS
    private BannerView bannerView;
    private InterstitialAd interstitial;
    private RewardBasedVideoAd rewardBasedVideo;

    public void Start()
    {
        this.rewardBasedVideo = RewardBasedVideoAd.Instance;
        this.rewardBasedVideo.OnAdLoaded += this.HandleRewardBasedVideoLoaded;
        this.rewardBasedVideo.OnAdFailedToLoad += this.HandleRewardBasedVideoFailedToLoad;
        this.rewardBasedVideo.OnAdOpening += this.HandleRewardBasedVideoOpened;
        this.rewardBasedVideo.OnAdStarted += this.HandleRewardBasedVideoStarted;
        this.rewardBasedVideo.OnAdRewarded += this.HandleRewardBasedVideoRewarded;
        this.rewardBasedVideo.OnAdClosed += this.HandleRewardBasedVideoClosed;
        this.rewardBasedVideo.OnAdLeavingApplication += this.HandleRewardBasedVideoLeftApplication;
    }

    // Returns an ad request with custom ad targeting.
    private AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder()
            .AddTestDevice(AdRequest.TestDeviceSimulator)
            .AddTestDevice(SystemInfo.deviceUniqueIdentifier)
            .AddKeyword("game")
            .SetGender(Gender.Male)
            .SetBirthday(new DateTime(1985, 1, 1))
            .TagForChildDirectedTreatment(false)
            .AddExtra("color_bg", "9B30FF")
            .Build();

    }
    [Header("Admob config")]
    string bannerAndroid; string bannerIOS;
    string interstitialAndroid = "ca-app-pub-7350837340790080/6993913255", interstitialIOS;
    string rewardAndroid = "ca-app-pub-7350837340790080/3054668246", rewardIOS;

    [SerializeField]
    bool autoCache = true;

    #region  Banner
    public void RequestBanner()
    {
        // These ad units are configured to always serve test ads.
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = bannerAndroid;
#elif UNITY_IPHONE
        string adUnitId = bannerIOS;
#else
        string adUnitId = "unexpected_platform";
#endif
        this.bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Top);

        // Register for ad events.
        this.bannerView.OnAdLoaded += this.HandleAdLoaded;
        this.bannerView.OnAdFailedToLoad += this.HandleAdFailedToLoad;
        this.bannerView.OnAdOpening += this.HandleAdOpened;
        this.bannerView.OnAdClosed += this.HandleAdClosed;
        this.bannerView.OnAdLeavingApplication += this.HandleAdLeftApplication;

        // Load a banner ad.
        this.bannerView.LoadAd(this.CreateAdRequest());
    }

    public void DestroyBanner()
    {
        this.bannerView.Destroy();
    }

    public void HideBanner()
    {
        this.bannerView.Hide();
    }

    public void ShowBanner()
    {
        this.bannerView.Show();
    }
    #endregion

    #region Interstitial
    bool interstitialAutoShow = false;
    AdsState interstitialState = AdsState.None;
    public void RequestInterstitial(bool autoShow)
    {
        if (this.interstitial != null && this.interstitial.IsLoaded())
        {
            interstitialState = AdsState.Loaded;
            if (autoShow)
                ShowInterstitial();
            return;
        }

        else if (interstitialState == AdsState.Loading)
        {
            if (this.interstitial != null && this.interstitial.IsLoaded()) interstitialState = AdsState.Loaded;
            interstitialAutoShow = autoShow;
            return;
        }
        string adUnitId = "unused";
#if UNITY_ANDROID
        adUnitId = interstitialAndroid;
#elif UNITY_IPHONE
        adUnitId = interstitialIOS;
#else
        adUnitId = "unexpected_platform";
#endif

        if (this.interstitial == null || !this.interstitial.IsLoaded())
        {
            // Create an interstitial.
            this.interstitial = new InterstitialAd(adUnitId);
            // Register for ad events.
            this.interstitial.OnAdLoaded += this.HandleInterstitialLoaded;
            this.interstitial.OnAdFailedToLoad += this.HandleInterstitialFailedToLoad;
            this.interstitial.OnAdOpening += this.HandleInterstitialOpened;
            this.interstitial.OnAdClosed += this.HandleInterstitialClosed;
            this.interstitial.OnAdLeavingApplication += this.HandleInterstitialLeftApplication;
            // Load an interstitial ad.
            this.interstitial.LoadAd(this.CreateAdRequest());
        }
        interstitialState = AdsState.Loading;
        interstitialAutoShow = autoShow;
    }

    public void ShowInterstitial()
    {
        if (this.interstitial.IsLoaded())
        {
            this.interstitial.Show();
            interstitialAutoShow = false;
            interstitialState = AdsState.None;
        }
    }


    public void DestroyInterstitial()
    {
        this.interstitial.Destroy();
    }
    #endregion

    #region Reward Based Video
    bool rewardVideoAutoShow = false;
    AdsState rewardState = AdsState.None;
    Action<bool, string> callBack;
    public void RequestRewardBasedVideo(bool autoShow, Action<bool, string> callBack = null)
    {
        this.callBack = callBack;
        if (rewardState == AdsState.Loaded && autoShow)
        {
            ShowRewardBasedVideo();
            return;
        }
        else if (rewardState == AdsState.Loading)
        {
            rewardVideoAutoShow = autoShow;
            return;
        }

        string adUnitId = "unused";
#if UNITY_ANDROID
        adUnitId = rewardAndroid;
#elif UNITY_IPHONE
        adUnitId = rewardIOS;
#else
        adUnitId = "unexpected_platform";
#endif
        this.rewardBasedVideo.LoadAd(this.CreateAdRequest(), adUnitId);
        rewardVideoAutoShow = autoShow;
    }

    public void ShowRewardBasedVideo()
    {
        if (this.rewardBasedVideo.IsLoaded())
        {
            this.rewardBasedVideo.Show();
            rewardState = AdsState.None;
            rewardVideoAutoShow = false;
        }
    }
    #endregion

    #region Banner callback handlers

    public void HandleAdLoaded(object sender, EventArgs args)
    {
        print("Banner: Đã load xong");
    }

    public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        print("Banner: Lỗi khi load");
    }

    public void HandleAdOpened(object sender, EventArgs args)
    {
        print("Banner: Đã hiển thị");
    }

    public void HandleAdClosed(object sender, EventArgs args)
    {
        print("Banner: Đã đóng");
    }

    public void HandleAdLeftApplication(object sender, EventArgs args)
    {
        print("Banner: Rời khỏi ứng dụng");
    }

    #endregion

    #region Interstitial callback handlers

    public void HandleInterstitialLoaded(object sender, EventArgs args)
    {
        print("Interstitial: Đã load xong");
        interstitialState = AdsState.Loaded;
        if (interstitialAutoShow) ShowInterstitial();
    }

    public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        print("Interstitial: Load lỗi");
        interstitialState = AdsState.Error;
    }

    public void HandleInterstitialOpened(object sender, EventArgs args)
    {
        print("Interstitial: Đã show");
        interstitialState = AdsState.None;
        Time.timeScale = 0;
        //RequestInterstitial(false);
    }

    public void HandleInterstitialClosed(object sender, EventArgs args)
    {
        print("Interstitial: Đã đóng");
        interstitialState = AdsState.None;
        Time.timeScale = 1;
        RequestInterstitial(false);
    }

    public void HandleInterstitialLeftApplication(object sender, EventArgs args)
    {
        print("Interstitial: Rời khỏi ứng dụng");
    }

    #endregion

    #region Reward Based Video callback handlers

    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        print("Reward Based Video: Đã load xong");
        rewardState = AdsState.Loaded;
        if (this.callBack != null)
            this.callBack(false, "");
        if (rewardVideoAutoShow) ShowRewardBasedVideo();
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        print("Reward Based Video: Load lỗi");
        rewardState = AdsState.Error;
        if (this.callBack != null)
            this.callBack(false, "error");

    }

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        print("Reward Based Video: Đã mở");
    }

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        print("Reward Based Video: Đã bắt đầu chạy");
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        print("Reward Based Video: Đã đóng");
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        print("Reward Based Video: Đã xem hoàn thành = Xử lý trả thưởng");
        if (this.callBack != null)
            this.callBack(true, "");
    }

    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        print("Reward Based Video: Rời khỏi ứng dụng");
    }

    #endregion
#endif

#if UNITY_WEBGL
    public void RequestRewardBasedVideo(bool autoShow, Action<bool, string> callBack = null)
    {

    }

    public void RequestInterstitial(bool autoShow)
    {

    }

    public void ShowInterstitial()
    {

    }

    [DllImport("__Internal")]
    private static extern void WShowInterstitial();

    [DllImport("__Internal")]
    private static extern void WShowRewardVideo();
#endif
}

enum AdsState { 
    None,
    Created,
    Loading,
    Loaded,
    Error
}

#if UNITY_WSA
public interface InterfaceController
{
    //INTERTITIAL
    void RequestIntertitial();
    void ShowIntertitial();
    void DestroyInterstitial();

    //REWARD VIDEO
    void RequestReward();
    void ShowReward();
    void DestroyReward();

    //BANNER
    void CreateBanner();
    void HideBanner();
    void ShowBanner();
}

public interface BackInterfaceController
{
    //interstitial
    void InterstitialLoaded();
    void InterstitialError();
    void InterstitialShowing();
    void InterstitialClosed();

    //reward
    void RewardLoaded();
    void RewardError();
    void RewardShowing();
    void RewardClosed();
    void RewardCompleted();
}
#endif