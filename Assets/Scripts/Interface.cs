﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


public class Interface : MonoBehaviour
{

    public Database database;
    public Balance balance;
    public GameObject investmnetsBoard;
    public GameObject investmentPrefab;

    public Text BalanceValueText;
    public Text AdditionalValueText;

    public GameObject shop;

    public GameObject acceptPanel;
    public GameObject MenuPanel;
    public GameObject moneyExchangePanel;
    public GameObject UpdatePCPanel;
    public GameObject settingsPanel;
    public GameObject statisticsPanel;
    public GameObject shopPanel;
    public GameObject appearancePanel;
    public GameObject petsPanel;
    public GameObject casinoPanel;
    public GameObject bonusesPanel;
    public GameObject roomSkinsPanel;

    public GameObject NotEnoughMoneyPanel;


    public GameObject clickWin;
    public GameObject modeButton;


    private bool AcceptBuy = false;
    private bool CancelBuy = false;

    public GameObject soundManager;
    public GameObject musicManager;

    public GameObject soundCheck;
    public GameObject musicCheck;

    public GPSManager gpsManager;

    public AppsFlyerManager tracker;

    void Start()
    {
        switch (PlayerPrefs.GetInt("Audio_Sound"))
        {
            case 1:

                soundManager.SetActive(false);
                soundCheck.SetActive(false);
                break;
            case 0:

                soundManager.SetActive(true);
                soundCheck.SetActive(true);
                break;
        }

        switch (PlayerPrefs.GetInt("Music_Sound"))
        {
            case 1:

                musicManager.SetActive(false);
                musicCheck.SetActive(false);
                break;
            case 0:

                musicManager.SetActive(true);
                musicCheck.SetActive(true);
                break;
        }


        Invoke("PlusBonusValues", 0.2f);


    }

    void PlusBonusValues()
    {
        print("1: " + balance.BalanceValue);
        balance.BalanceValue += PlayerPrefs.GetInt("BonusGameValue");
        print("2: " + balance.BalanceValue);
        PlayerPrefs.SetInt("BonusGameValue", 0);
    }

    void FixedUpdate()
    {
        UpgradeInvestmentsTile();
        UpdateBalanceValues();
    }

    public void ChangeBalanceValueText(long value)
    {
        BalanceValueText.text = FormattingValue(value);
    }

    public void UpdateInvestment(int index)
    {
        string investmentType = database.GetInvestmentTypeByIndex(index);
        int investmentLevel = database.GetInvestmentLevel(investmentType);
        long investmentUpgradeCost = database.GetInvestmentUpgradeCost(investmentType, investmentLevel);

        if (balance.BalanceValue >= investmentUpgradeCost)
        {
            if (investmentLevel != database.maxInvestmentLevel)
            {
                ShowAcceptPanel();
                StartCoroutine(UpgradeStart(index));
            }
        }
        else
        {
            NotEnoughMoneyPanel.SetActive(true);
            Button notEnoughBtn = NotEnoughMoneyPanel.GetComponentInChildren<Button>();
            notEnoughBtn.onClick.RemoveAllListeners();
            notEnoughBtn.onClick.AddListener(() =>
            {
                NotEnoughMoneyPanel.SetActive(false);
                bonusesPanel.SetActive(true);
            });
        }
    }

    private void UpdateBalanceValues()
    {
        string value = FormattingValue(balance.BalanceValue);
        BalanceValueText.text = value;

        value = FormattingValue(balance.AdditionalValue);
        AdditionalValueText.text = value + "/sec";

    }

    private IEnumerator UpgradeStart(int index)
    {
        yield return new WaitUntil(() => AcceptBuy == true || CancelBuy == true);

        if (AcceptBuy)
        {
            string investmentType = database.GetInvestmentTypeByIndex(index);
            int investmentLevel = database.GetInvestmentLevel(investmentType);
            long investmentUpgradeCost = database.GetInvestmentUpgradeCost(investmentType, investmentLevel);

            if (investmentLevel < database.maxInvestmentLevel)
            {


                if (GameObject.FindGameObjectWithTag("Sound") != null)
                {
                    AudioManager.Instance.PlayAudio(AudioManager.Instance.upgradeInvest);
                }

                database.UpgradeInvestment(investmentType);
                balance.BalanceValue -= investmentUpgradeCost;
                StatisticsController.Instance.IncreaseSpendMoney(investmentUpgradeCost);
                balance.NewAdditionalIncome();
            }
        }
    }

    private void UpgradeInvestmentsTile()
    {

        for (int i = 0; i < database.investmentCatalog.Count; i++)
        {
            database.investmentTiles[i].name.text = database.investmentCatalog[i].type;

            int level = database.myInvestments[i].level;
            database.investmentTiles[i].level.text = level.ToString();

            // *** Unlocked Achievements ***

            // ---- Invest Achievements ----
            if (database.myInvestments[0].level == 50 && PlayerPrefs.GetInt("INVEST_10") == 0) // 1 invest to 50 level 
            {
                gpsManager.GetAchievement(GPGSIds.achievement_investor_1);
                HardCurrencyManager.hardCurrencyValue += 5;
                PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
                PlayerPrefs.SetInt("INVEST_10", 1);
            }
            if (database.myInvestments[4].level == 50 && PlayerPrefs.GetInt("INVEST_30") == 0)    // 5 invest to 50 level
            {
                gpsManager.GetAchievement(GPGSIds.achievement_investor_2);
                HardCurrencyManager.hardCurrencyValue += 10;
                PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
                PlayerPrefs.SetInt("INVEST_30", 1);
            }
            if (database.myInvestments[7].level == 50 && PlayerPrefs.GetInt("INVEST_50") == 0)    // 8 invest to 50 level
            {
                gpsManager.GetAchievement(GPGSIds.achievement_investor_3);
                HardCurrencyManager.hardCurrencyValue += 40;
                PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
                PlayerPrefs.SetInt("INVEST_50", 1);
            }
            //------------------------------

            // ---- Balance Achievements ----
            if (balance.BalanceValue >= 1000 && PlayerPrefs.GetInt("BALANCE_1_KB") == 0)
            {
                gpsManager.GetAchievement(GPGSIds.achievement_traffic_1);
                HardCurrencyManager.hardCurrencyValue += 5;
                PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
                PlayerPrefs.SetInt("BALANCE_1_KB", 1);
            }
            if (balance.BalanceValue >= 1000000 && PlayerPrefs.GetInt("BALANCE_1_MB") == 0)
            {
                tracker.MoneyTracking();
                gpsManager.GetAchievement(GPGSIds.achievement_traffic_2);
                HardCurrencyManager.hardCurrencyValue += 10;
                PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
                PlayerPrefs.SetInt("BALANCE_1_MB", 1);
            }
            if (balance.BalanceValue >= 1000000000 && PlayerPrefs.GetInt("BALANCE_1_GB") == 0)
            {
                gpsManager.GetAchievement(GPGSIds.achievement_traffic_3);
                HardCurrencyManager.hardCurrencyValue += 15;
                PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
                PlayerPrefs.SetInt("BALANCE_1_GB", 1);
            }
            if (balance.BalanceValue >= 1000000000000 && PlayerPrefs.GetInt("BALANCE_1_TB") == 0)
            {
                gpsManager.GetAchievement(GPGSIds.achievement_traffic_4);
                HardCurrencyManager.hardCurrencyValue += 20;
                PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
                PlayerPrefs.SetInt("BALANCE_1_TB", 1);
            }
            // ------------------------------

            // *****************************


            if (level != database.maxInvestmentLevel)
            {
                string price = FormattingValue(database.investmentCatalog[i].upgradeCost[level].cost);
                database.investmentTiles[i].price.text = price;

                string value = FormattingValue(database.investmentCatalog[i].levelBonus[level + 1].bonusIncome);
                database.investmentTiles[i].value.text = value;
            }
            else
            {
                database.investmentTiles[i].price.text = "Max";
                database.investmentTiles[i].value.text = "Max";
                database.investmentTiles[i].image.transform.parent.Find("Invest").gameObject.SetActive(false);
            }


            int index = database.GetMyInvestmentIndex(database.investmentCatalog[i].type);
            long earning = database.myInvestments[index].bonusIncome;
            database.investmentTiles[i].earning.text = FormattingValue(earning);
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ModeBtn()
    {
        clickWin.SetActive(!clickWin.activeSelf);
        modeButton.SetActive(!modeButton.activeSelf);
    }

    public void ActiveShop()
    {
        MenuPanel.SetActive(!MenuPanel.activeSelf);
        shop.SetActive(!shop.activeSelf);
    }

    public void ActiveBonuses()
    {
        MenuPanel.SetActive(false);
        bonusesPanel.SetActive(!bonusesPanel.activeSelf);
    }

    public void ActiveRoomSkinsPanel()
    {
        MenuPanel.SetActive(false);
        roomSkinsPanel.SetActive(!roomSkinsPanel.activeSelf);
    }

    public void ActivePets()
    {
        MenuPanel.SetActive(false);
        petsPanel.SetActive(!petsPanel.activeSelf);
    }

    public void ActiveCasinoPanel()
    {
        MenuPanel.SetActive(!MenuPanel.activeSelf);
        casinoPanel.SetActive(!casinoPanel.activeSelf);
    }

    public void ActiveAppearancePanel()
    {
        MenuPanel.SetActive(false);


        appearancePanel.SetActive(!appearancePanel.activeSelf);
    }

    public void ActiveSettings()
    {
        MenuPanel.SetActive(!MenuPanel.activeSelf);
        settingsPanel.SetActive(!settingsPanel.activeSelf);
    }
    public void ActiveStatistics()
    {
        MenuPanel.SetActive(!MenuPanel.activeSelf);
        statisticsPanel.SetActive(!statisticsPanel.activeSelf);
    }
    public void ActiveMoneyExchanhe()
    {
        MenuPanel.SetActive(!MenuPanel.activeSelf);
        moneyExchangePanel.SetActive(!moneyExchangePanel.activeSelf);
    }

    public void ActiveUpdatePC()
    {
        MenuPanel.SetActive(!MenuPanel.activeSelf);
        UpdatePCPanel.SetActive(!UpdatePCPanel.activeSelf);
    }


    public void ActiveMenu()
    {
        MenuPanel.SetActive(!MenuPanel.activeSelf);
        shop.SetActive(false);
    }

    public void ActiveShopPanel()
    {
        MenuPanel.SetActive(!MenuPanel.activeSelf);
        shopPanel.SetActive(!shopPanel.activeSelf);
    }

    public void CloseButton()
    {
        MenuPanel.SetActive(false);
        shop.SetActive(false);
        moneyExchangePanel.SetActive(false);
        UpdatePCPanel.SetActive(false);
        settingsPanel.SetActive(false);
        statisticsPanel.SetActive(false);
        shopPanel.SetActive(false);
        appearancePanel.SetActive(false);
        petsPanel.SetActive(false);
        bonusesPanel.SetActive(false);
        roomSkinsPanel.SetActive(false);
    }

    public void ZeroAllValues()
    {
        balance.BalanceValue = 70;
        balance.AdditionalValue = 0;
        balance.TaplValue = 5;
        balance.WriteBalanceValues();
        database.ZeroMyInvestments();
        balance.NewAdditionalIncome();
    }

    public void ShowAcceptPanel()
    {
        AcceptBuy = false;
        CancelBuy = false;
        acceptPanel.SetActive(true);
    }

    public void AcceptButton()
    {
        AcceptBuy = true;
        CloseAcceptPanel();
    }

    public void CancelButton()
    {
        CancelBuy = true;
        CloseAcceptPanel();
    }

    public void CloseAcceptPanel()
    {
        acceptPanel.SetActive(false);
    }

    public string FormattingValue(long value)
    {
        string newText, ending = "";

        long bigPart = value;
        long smallPart = 0;

        if (value > 1000000000000000)
        {
            smallPart = bigPart % 1000000000000000;
            smallPart /= 10000000000000;
            bigPart /= 1000000000000000;
            ending = " Pb";
        }
        else if (value >= 1000000000000)
        {
            smallPart = bigPart % 1000000000000;
            smallPart /= 10000000000;
            bigPart /= 1000000000000;
            ending = " Tb";
        }
        else if (value >= 1000000000)
        {
            smallPart = bigPart % 1000000000;
            smallPart /= 10000000;
            bigPart /= 1000000000;
            ending = " Gb";
        }
        else if (value >= 1000000)
        {
            smallPart = bigPart % 1000000;
            smallPart /= 10000;
            bigPart /= 1000000;
            ending = " Mb";
        }
        else if (value >= 1000)
        {
            smallPart = bigPart % 1000;
            smallPart /= 100;
            bigPart /= 1000;
            ending = " Kb";
        }
        else
        {
            ending = " B";
        }

        newText = bigPart.ToString() + "," + smallPart.ToString() + " " + ending;

        return newText;
    }

    public void OpenSceneBtn(string scneName)
    {
        PlayerPrefs.SetString("LoadLevel", scneName);
        SceneManager.LoadScene("Loader");
        
    }

    // *** Audio Checks ***

    public void SuondToggle()
    {
        switch (PlayerPrefs.GetInt("Audio_Sound"))
        {
            case 0:

                soundManager.SetActive(false);
                soundCheck.SetActive(false);

                PlayerPrefs.SetInt("Audio_Sound", 1);
                break;
            case 1:

                soundManager.SetActive(true);
                soundCheck.SetActive(true);

                PlayerPrefs.SetInt("Audio_Sound", 0);
                break;
        }


    }

    public void MusicToggle()
    {
        switch (PlayerPrefs.GetInt("Music_Sound"))
        {
            case 0:

                musicManager.SetActive(false);
                musicCheck.SetActive(false);
                PlayerPrefs.SetInt("Music_Sound", 1);
                break;
            case 1:

                musicManager.SetActive(true);
                musicCheck.SetActive(true);

                PlayerPrefs.SetInt("Music_Sound", 0);
                break;
        }


    }
    // ********************

    // Set curret balance valut

    public void SetCurrentBalanceValue()
    {
        string s = balance.AdditionalValue.ToString();
        PlayerPrefs.SetString("CurrentBalanceValue", s);
    }

    //*********************
}
