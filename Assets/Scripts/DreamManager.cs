﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DreamManager : MonoBehaviour {

    private string[] dreamText =
    {
        "Not bad, not bad...",
        "It is necessary to work more!",
        "Let's invest in one more project!",
        "Good day, to start something new!",
        "I would have lunch, though... No, I will not, it's better to earn more coins!",
        "Crypto-coin, crypto-coin! Buy whatever, you can pay with coin!",
        "I'm bored with old furniture. I have to look for something new!",
        "There is a belief that pets bring good luck. I need to get one... or two!",
        "I am sure, today is gonna be a day with good profit!"

    };

    public GameObject dreamObject;
    public Text dreaTextComponent;

	void Start () {

        dreamObject.SetActive(false);

        InvokeRepeating("ShowDream", 5f, 60f);
	}
	
	
	private void ShowDream () {

        int randDream = Random.Range(0, dreamText.Length);
        dreamObject.SetActive(true);
        dreaTextComponent.text = dreamText[randDream].ToString();
        Invoke("HideDream", 3f);
   
    }

    private void HideDream ()
    {
        dreamObject.SetActive(false);
    }
}
