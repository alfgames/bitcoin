﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireAlarmManager : MonoBehaviour {

    private Balance balance;
    public  Canvas fireCanvas;

    private int clickCount_1;
    private int clickCount_2;
    private int clickCount_3;

    void Start () {

        clickCount_1 = 0;
        clickCount_2 = 0;
        clickCount_3 = 0;

        fireCanvas.worldCamera = Camera.main;

        balance = GameObject.Find("Balance").GetComponent<Balance>();

        InvokeRepeating("MinusBalance", 0, 1f);
	}
	
	
	void Update () {
        if (clickCount_1 > 2 && clickCount_2 > 2 && clickCount_3 > 2)
        {
            PlayerPrefs.SetInt("TimerState", 0);
            Destroy(gameObject);
        }
	}

    private void MinusBalance()
    {
        if (balance.BalanceValue > 0)
        {
            balance.BalanceValue -= (balance.BalanceValue / 7);
        }
    }

    public void ClickFireBtn(GameObject fireSprite)
    {
        switch (fireSprite.name)
        {
            case "Sprite_1":
                clickCount_1++;
                if (clickCount_1 == 3)
                {
                    fireSprite.SetActive(false);
                }
                break;
            case "Sprite_2":
                clickCount_2++;
                if (clickCount_2 == 3)
                {
                    fireSprite.SetActive(false);
                }
                break;
            case "Sprite_3":
                clickCount_3++;
                if (clickCount_3 == 3)
                {
                    fireSprite.SetActive(false);
                }
                break;
        }
    }
}
