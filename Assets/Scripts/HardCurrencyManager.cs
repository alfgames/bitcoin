﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HardCurrencyManager : MonoBehaviour {

    public Text hardCurrencyText;

    private static int _hardCurrencyValue;

    public static int hardCurrencyValue
    {
        get { return _hardCurrencyValue; }
        set
        {
            _hardCurrencyValue = value;
            PlayerPrefs.SetInt("HardCurrencyValue", _hardCurrencyValue);
        }
    }

	void Start () {





        hardCurrencyValue = PlayerPrefs.GetInt("HardCurrencyValue");
    }
	
	void Update () {
        hardCurrencyText.text = _hardCurrencyValue.ToString();
    }
}
