﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

    //public enum InvestmentType
    //{
    //    typeA = 0,
    //    typeB,
    //    typeC,
    //    typeD,
    //    typeE
    //}

    public struct Investment
    {
        public string type;
        public List<InvestmentUpgrade> upgradeCost;
        public List<InvestmentLevelBonus> levelBonus;

        public Investment(string type, List<InvestmentUpgrade> upgradeCost,  List<InvestmentLevelBonus> levelBonus)
        {
            this.type = type;
            this.upgradeCost = upgradeCost;
            this.levelBonus = levelBonus;
        }
    }

    public struct InvestmentUpgrade
    {
        public int level;
        public long cost;

        public InvestmentUpgrade(int level, long cost)
        {
            this.level = level;
            this.cost = cost;
        }
    }

    public struct InvestmentLevelBonus
    {
        public int level;
        public long bonusIncome;

        public InvestmentLevelBonus(int level, long bonusIncome)
        {
            this.level = level;
            this.bonusIncome = bonusIncome;
        }
    }

    public struct myInvestment 
    { 
        public string type;
        public int level;
        public long bonusIncome;

        public myInvestment(string type, int level, long bonusIncome)
        {
            this.type = type;
            this.level = level;
            this.bonusIncome = bonusIncome;
        }
    }

    public struct InvestmentTile
    {
        public Text name;
        public Text level;
        public Text price;
        public Text value;
        public Text earning;
        public Image image;

        public InvestmentTile(Text name, Text level, Text price, Text value, Text earning, Image image)
        {
            this.name = name;
            this.level = level;
            this.price = price;
            this.value = value;
            this.earning = earning;
            this.image = image;
        }
    }

    public struct Farming
    {
        public int percent;
        public int time;

        public Farming(int percent, int time)
        {
            this.percent = percent;
            this.time = time;
        }
    }


