﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpinControl : MonoBehaviour {

    public int timeNowMiliSec;
    public int timeNowSec;
    public int timeNowMin;
    public int timeNowHour;

    public static int TIME_FOR_FREE_SPIN = 1440; // в минутах 24 часа = 1440
    public static int PRICE_SPIN_FOR_HARD = 4;

    float tempTime;

    public static int TimerMinute;
    public Text TextTimerMinute;

    public static int TimerSeconds;

    public Interface myInterface;

    //-------------------------------
    public Button buttonFreeSpin;
    public Button buttonSpinForHard;
    public Sprite openFreeSpin;
    public Sprite closeSpin;
    public Sprite openSpinForHard;
    //-------------------------------
    public GameObject panelQustion;
    public Text textQuestion;
    public Button yesQuestion;
    public Button noQuestion;

    public string questionGoSpin = "Вы уверены, что хотите купить спин за 4 харды?";
    public string questionNoNard = "предложением перенаправить его в магазин покупки харды";
    //-------------------------------


    public BonusControl bonusControl;
    public ScrollRectControl scrollRectControl;

    void Start () {
        HowTimeNow();

        if ( PlayerPrefs.GetFloat("LastTimeExit") != 0)
        {
            if (PlayerPrefs.GetFloat("NowPointTime") > PlayerPrefs.GetFloat("LastTimeExit"))//если не перезаг, то все ок
            {
                if ((PlayerPrefs.GetFloat("NowPointTime") - PlayerPrefs.GetFloat("LastTimeExit")) > TIME_FOR_FREE_SPIN)
                {
                    FreeSpinOpen();
                    PlayerPrefs.SetInt("NextPointTime", (timeNowMin + TIME_FOR_FREE_SPIN));
                }
                else
                {
                    FreeSpinClose();
                }
            }
            else
            {
                PlayerPrefs.SetFloat("LastTimeExit", PlayerPrefs.GetFloat("NowPointTime"));
                FreeSpinClose();
            }
        }
        else
        {
            PlayerPrefs.SetFloat("LastTimeExit", PlayerPrefs.GetFloat("NowPointTime"));
            FreeSpinClose();
        }

        SpinForHardOpen();
        panelQustion.SetActive(false);
    }
	
	void Update () {
        HowTimeNow();

        if (PlayerPrefs.GetFloat("NowPointTime") >= PlayerPrefs.GetInt("NextPointTime"))
        {
            FreeSpinOpen();
        }

        if ((PlayerPrefs.GetInt("NextPointTime") - PlayerPrefs.GetFloat("NowPointTime")) < TIME_FOR_FREE_SPIN)
            tempTime = PlayerPrefs.GetInt("NextPointTime") - PlayerPrefs.GetFloat("NowPointTime");
        else
        {
            tempTime = PlayerPrefs.GetInt("NextPointTime") - PlayerPrefs.GetFloat("NowPointTime");
        }
        if (tempTime < 0) tempTime = 0;
        TimerMinute = (int)(tempTime / 60);             //записываем целые в минуты
        TimerSeconds = (int)(tempTime % 60);            //записываем остаток в секунды
        TextTimerMinute.text = (TimerMinute.ToString() + ":" + TimerSeconds.ToString());
    }

    void HowTimeNow()
    {
        timeNowMiliSec = System.Environment.TickCount; // получаем время в милисекундах
        timeNowSec = timeNowMiliSec / 1000; // переводим в секунды
        timeNowMin = timeNowSec / 60; // переводим в минуты
        timeNowHour = timeNowMin / 60; // часы

        PlayerPrefs.SetFloat("NowPointTime", timeNowMin);
    }

    void FreeSpinOpen()
    {
        buttonFreeSpin.GetComponent<Image>().sprite = openFreeSpin;
        buttonFreeSpin.GetComponent<Image>().color = new Color(1, 1, 1, 1);
        buttonFreeSpin.GetComponent<Button>().enabled = true;
    }
    void SpinForHardOpen()
    {
        buttonSpinForHard.GetComponent<Image>().sprite = openSpinForHard;
        buttonSpinForHard.GetComponent<Image>().color = new Color(1, 1, 1, 1);
        buttonSpinForHard.GetComponent<Button>().enabled = true;
    }

    void FreeSpinClose()
    {
        buttonFreeSpin.GetComponent<Image>().sprite  = closeSpin;
        buttonFreeSpin.GetComponent<Image>().color = new Color(1, 1, 1, 1);
        buttonFreeSpin.GetComponent<Button>().enabled = false;

        buttonSpinForHard.GetComponent<Image>().sprite = closeSpin;
        buttonSpinForHard.GetComponent<Image>().color = new Color(1, 1, 1, 1);
        buttonSpinForHard.GetComponent<Button>().enabled = false;
    }
    //-----------------------------
    public void ButtonSpin()
    {
        PlayerPrefs.SetInt("NextPointTime", (timeNowMin + TIME_FOR_FREE_SPIN));
        FreeSpinClose();
        Spin();
    }

    public void ButtonSpinForHard()
    {
        if (PlayerPrefs.GetInt("HardCurrencyValue") >= PRICE_SPIN_FOR_HARD) {
            panelQustion.SetActive(true);
            textQuestion.text = questionGoSpin;
        }
        else
        {
            panelQustion.SetActive(true);
            textQuestion.text = questionNoNard;
        }
    }

    void Spin()
    {
        System.Random rand = new System.Random();
        float randTime = rand.Next(2000, 3000);
        float timeForSpin = randTime / 1000;

        StartCoroutine(Spin(timeForSpin));
    }

    IEnumerator Spin(float time)
    {
        ScrollRectControl.move = true;
        yield return new WaitForSeconds(time);
        ScrollRectControl.move = false;
        bonusControl.ActivateBonus(scrollRectControl.GetNameBonus());
        SpinForHardOpen();
    }
    //-------------------------------

    public void ButtonYesQuestion()
    {
        if(textQuestion.text == questionGoSpin)
        {
            panelQustion.SetActive(false);
            HardCurrencyManager.hardCurrencyValue -= PRICE_SPIN_FOR_HARD;
            PlayerPrefs.SetInt("HardCurrencyValue", HardCurrencyManager.hardCurrencyValue);
            Spin();
            FreeSpinClose();
        }
        else if(textQuestion.text == questionNoNard)
        {
            panelQustion.SetActive(false);
            myInterface.ActiveCasinoPanel();
            myInterface.ActiveMoneyExchanhe();
            Debug.Log("OPEN SCENE WITH BUY MONEY");
        }
        //panelQustion.SetActive(false);
    }

    public void ButtonNoQuestion()
    {
        panelQustion.SetActive(false);
    }

    void OnApplicationPause()
    {
        PlayerPrefs.SetFloat("LastTimeExit", timeNowMin);
    }

    void OnApplicationQuit()
    {
        PlayerPrefs.SetFloat("LastTimeExit", timeNowMin);
    }
}
