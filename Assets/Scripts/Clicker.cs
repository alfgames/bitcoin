﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Clicker : MonoBehaviour {

	public int  Click_gpa;
	public GameObject ClickUI;

    public Balance balance;

    private void Start()
    {
        Click_gpa = balance.TaplValue;    
    }

    void Update () {

		Vector3 aTapPoint = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		Collider2D aCollider2d = Physics2D.OverlapPoint (aTapPoint);

		if (aCollider2d) {
			GameObject obj = aCollider2d.transform.gameObject;
				if (Input.GetMouseButtonDown (0)) {
                Click_gpa = balance.TaplValue;
                GameObject ClickUIObj = Instantiate (ClickUI, new Vector3 (aTapPoint.x, aTapPoint.y, 0), Quaternion.identity);
					ClickUIObj.transform.Find("Text").GetComponent<Text> ().text = "+" + (Click_gpa*0.01).ToString ();
                ClickClick();
                Destroy (ClickUIObj, 1);
			}
		}
    }

    public void ClickClick()
    {
        Click_gpa = balance.TaplValue;
        balance.BalanceValue += balance.TaplValue;
        StatisticsController.Instance.IncreaseEarnedMoney(balance.TaplValue);
        StatisticsController.Instance.IncreaseTapAmount();
    }
}
