using UnityEngine;
using System.Collections;
//using System.IO;
//using System.Runtime.InteropServices;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SocialManager : MonoBehaviour {

    string subject = "Subject text";
    string body = "This is a great game! Download it soon https://play.google.com/store/apps/details?id=";
    public Image musicImage;
    public Sprite[] musicSprite;

    private Scene currentScene;

//#if UNITY_IPHONE
	
//	[DllImport("__Internal")]
//	private static extern void sampleMethod (string iosPath, string message);
	
//	[DllImport("__Internal")]
//	private static extern void sampleTextMethod (string message);
	
//#endif
    public GameObject musicManager;

    private void Start()
    {
        body = "This is a great game! Download it soon https://play.google.com/store/apps/details?id=" + Application.identifier;
        if (currentScene.name == "StartScene")
        {
            switch (PlayerPrefs.GetInt("Music_Sound"))
            {
                case 1:
                    musicImage.sprite = musicSprite[1];
                    musicManager.SetActive(false);
                    break;
                case 0:
                    musicImage.sprite = musicSprite[0];
                    musicManager.SetActive(true);
                    break;
            }

        }
    }

    public void OnAndroidTextSharingClick()
    {

        StartCoroutine(ShareAndroidText());

    }
    IEnumerator ShareAndroidText()
    {
        yield return new WaitForEndOfFrame();
        //execute the below lines if being run on a Android device
#if UNITY_ANDROID
        //Reference of AndroidJavaClass class for intent
        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
        //Reference of AndroidJavaObject class for intent
        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
        //call setAction method of the Intent object created
        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
        //set the type of sharing that is happening
        intentObject.Call<AndroidJavaObject>("setType", "text/plain");
        //add data to be passed to the other activity i.e., the data to be sent
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), subject);
        //intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TITLE"), "Text Sharing ");
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), body);
        //get the current activity
        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
        //start the activity by sending the intent data
        AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Share Via");
        currentActivity.Call("startActivity", jChooser);
#endif
    }


    public void OniOSTextSharingClick()
    {

#if UNITY_IPHONE || UNITY_IPAD
		string shareMessage = "Wow I Just Share Text ";
		//sampleTextMethod (shareMessage);
		
#endif
    }

    public void RateUs()
    {
#if UNITY_ANDROID
        Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
#elif UNITY_IPHONE
		Application.OpenURL("itms-apps://itunes.apple.com/app/idYOUR_ID");
#endif
    }

    public void MoreGames()
    {
#if UNITY_ANDROID
        //Application.OpenURL("https://play.google.com/store/apps/developer?id=Ex+App");
#endif
    }

    public void ExitBtn()
    {
        Application.Quit();
    }

    public void MusicBtn()
    {
        switch (PlayerPrefs.GetInt("Music_Sound"))
        {
            case 0:
                musicManager.SetActive(false);
                musicImage.sprite = musicSprite[1];

                PlayerPrefs.SetInt("Music_Sound", 1);
                PlayerPrefs.SetInt("Audio_Sound", 1);
                break;
            case 1:

                musicManager.SetActive(true);
                musicImage.sprite = musicSprite[0];

                PlayerPrefs.SetInt("Music_Sound", 0);
                PlayerPrefs.SetInt("Audio_Sound", 0);
                break;
        }
    }

    public void OpenPolicy()
    {
        Application.OpenURL("http://shooterboy.com/#privacy-policy");
    }
}
