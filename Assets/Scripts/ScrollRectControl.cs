﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Threading;

public class ScrollRectControl : MonoBehaviour
{
    public RectTransform panelScroll;
    public Image[] bttn;
    public RectTransform center;

    private float[] distance; // all butt distance to the ctnter
    private float[] distReposition;
    private bool dragging = false; // перетаскивание
    private int bttnDistance; // distance between the butt
    private int minButtonNum; // hold the number of the button with smallast distancr to center

    public static bool move = false;

    void Start()
    {
        distance = new float[bttn.Length];
        distReposition = new float[bttn.Length];

        bttnDistance = (int)Mathf.Abs(bttn[1].GetComponent<RectTransform>().anchoredPosition.y - bttn[0].GetComponent<RectTransform>().anchoredPosition.y);
    }

    void Update()
    {
        for (int i = 0; i < bttn.Length; i++)
        {
            distReposition[i] = center.GetComponent<RectTransform>().position.y - bttn[i].GetComponent<RectTransform>().position.y;
            distance[i] = Mathf.Abs(distReposition[i]);
            if( distReposition[i] < -12)
            {
                float curX = bttn[i].GetComponent<RectTransform>().anchoredPosition.x;
                float curY = bttn[i].GetComponent<RectTransform>().anchoredPosition.y;

                Vector2 newAnchoredPosition = new Vector2(curX, curY - (bttn.Length * bttnDistance));
                bttn[i].GetComponent<RectTransform>().anchoredPosition = newAnchoredPosition;
            }

            if (distReposition[i] > 12) // - on +
            {
                float curX = bttn[i].GetComponent<RectTransform>().anchoredPosition.x;
                float curY = bttn[i].GetComponent<RectTransform>().anchoredPosition.y;

                Vector2 newAnchoredPosition = new Vector2(curX, curY + (bttn.Length * bttnDistance)); // + on -
                bttn[i].GetComponent<RectTransform>().anchoredPosition = newAnchoredPosition;
            }
        }

        float minDistance = Mathf.Min(distance); //get min distace

        for (int a = 0; a < bttn.Length; a++)
        {
            if (minDistance == distance[a])
            {
                minButtonNum = a;
            }
        }

        if (move)
        {
            dragging = true;
            var speed = 15;
            panelScroll.transform.Translate(Vector3.down * Time.deltaTime * speed);
        }
        else
        {
            EndDrag();
            LerpToBttn(-bttn[minButtonNum].GetComponent<RectTransform>().anchoredPosition.y);
        }
    }
    void LerpToBttn(float position)
    {
        float newY = Mathf.Lerp(panelScroll.anchoredPosition.y, position, Time.deltaTime * 10f);
        Vector2 newPosition = new Vector2(0, newY);

        panelScroll.anchoredPosition = newPosition;
    }

    public void StartDrag()
    {
        dragging = true;
    }

    public void EndDrag()
    {
        dragging = false;
    }

    public string GetNameBonus()
    {
        //Debug.Log(bttn[minButtonNum].name);
        return bttn[minButtonNum].name;
    }
}
